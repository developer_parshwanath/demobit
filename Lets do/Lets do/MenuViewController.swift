//
//  MenuViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var menutableView:UITableView!
    var userLogin:UserLogin?
    var menuArray:[[String:String]] = [ ["name":"My Profile","icon":"MyProfile"],
                      ["name":"User Management","icon":"UserMangement"],
                      ["name":"Logout","icon":"Logout"]
    
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setUptableView()
        // Do any additional setup after loading the view.
    }
}

extension MenuViewController:UITableViewDelegate,UITableViewDataSource
{
    func setUptableView()  {
        self.menutableView.delegate = self
        self.menutableView.dataSource = self
        
        self.menutableView.register(UITableViewCell.self, forCellReuseIdentifier: "menuTableViewCell")
        
        self.menutableView.register(MenuProfileHeaderViewCell.nib, forHeaderFooterViewReuseIdentifier: MenuProfileHeaderViewCell.reuseIdentifier)
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

       
        guard let view = tableView.dequeueReusableHeaderFooterView(
                            withIdentifier: MenuProfileHeaderViewCell.reuseIdentifier)
                            as? MenuProfileHeaderViewCell
        else {
            return nil
        }
        view.setUPView(name: "", imageURL: "", userLogin: userLogin!)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath)
        
        let menuDict:[String:String] = self.menuArray[indexPath.row]
        cell.imageView?.image = UIImage(named: menuDict["icon"] ?? "")
        cell.imageView?.sizeToFit()
        cell.textLabel?.text = menuDict["name"]
        
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 14.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let menuDict:[String:String] = self.menuArray[indexPath.row]
        
        let name:String = menuDict["name"] ?? ""
        switch name {
        case "My Profile":
                let profileVC:ProfileViewController = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
                self.navigationController?.pushViewController(profileVC, animated: true)
                return
            case "User Management":
                let userMVC:UserManagementViewController = UserManagementViewController(nibName: userManagementViewController, bundle: nil)
                self.navigationController?.pushViewController(userMVC, animated: true)
                return
            case "Logout":
                self.showSimpleAlert()
            return
        default:
            return
        }
        
       
        
        
    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: "Logout?", message: "Are sure you want to logout",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.setRootControler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func setRootControler()
       {
           let taskVC:LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
           let navigationControler:UINavigationController = UINavigationController(rootViewController: taskVC)
           self.view.window?.rootViewController = taskVC
           self.view.window?.makeKeyAndVisible()
           
       }
}
