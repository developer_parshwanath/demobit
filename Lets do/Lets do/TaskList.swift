//
//  TaskList.swift
//  Lets do
//
//  Created by Darshan Bagmar on 17/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation
struct TaskLists {
    var orgId:String?
    var userName:String?
    var status:String?
    var createdBy:String?
    var assignedTo:String?
    var connectedContact:String?
    var pageNumber:Int?
    var pageSize:Int?
    
    /*createdBy= dipalikakade@gmail.com (optional)
    assignedTo = sridharan1984@gmail.com (optional)
    connectedContact = sridharan1984@gmail.com (optional)
*/
    
    func encodeRequest() -> [String:Any]
    {
        return [
        "orgId":orgId ?? "",
        "username":userName ?? "",
        "status":status ?? "",
        "createdBy":createdBy ?? "",
        "assignedTo":assignedTo ?? "",
        "connectedContact":connectedContact ?? "",
        "pageSize":pageSize ?? 0,
        "pageNumber":pageNumber ?? 0
           
        ]
        
    }
}


struct TaskList:Hashable {
    
    
    var taskId:String?
    var orgToken:String?
    var createdBy:String?
    var taskNumber:String?
    var assignee:String?
    var assigneeName:String?
    var priority:String?
    var status:String?
    var taskName:String?
    var taskDesc:String?
    var dueDate:String?
    var completionDate:String?
    var createdDate:String?
    var attachments:[Attachments]?
    var comments:[Comments]?
    var hashValue: Int { get { return taskId.hashValue } }
    
   
    
    static func decodeResponse(responseData:[String:Any]?)-> [TaskList]?
    {
        guard responseData != nil else{return nil}
        guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
        var userArray:[TaskList] = []
        for dataDict in userDetailArray {
            var userLoginDetails = TaskList()
            
            userLoginDetails.taskId = dataDict["taskId"] as? String
            userLoginDetails.orgToken = dataDict["orgToken"] as? String
            userLoginDetails.createdBy = dataDict["createdBy"] as? String
            userLoginDetails.taskNumber = dataDict["taskNumber"] as? String
            userLoginDetails.assignee = dataDict["assignee"] as? String
            userLoginDetails.assigneeName = dataDict["assigneeName"] as? String
            userLoginDetails.priority = dataDict["priority"] as? String
            userLoginDetails.status = dataDict["status"] as? String
            userLoginDetails.taskName = dataDict["taskName"] as? String
            userLoginDetails.taskDesc = dataDict["taskDesc"] as? String
            userLoginDetails.dueDate = dataDict["dueDate"] as? String
            userLoginDetails.completionDate = dataDict["completionDate"] as? String
            userLoginDetails.createdDate = dataDict["createdDate"]  as? String
            
            userLoginDetails.attachments = TaskList.Attachments.decodeResponse(responseData: dataDict["attachments"] as? [[String : Any]] ?? [])
            userLoginDetails.comments = TaskList.Comments.decodeResponse(responseData: dataDict["comments"] as? [[String : Any]] ?? [])
                //dataDict["comments"] as? [[String:Any]]
            
            
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
    
    struct Attachments:Hashable {
        var type:String?
        var fileName:String?
        var name:String?
        var number:String?
        var fileLink:String?
        
      
        
        static func decodeResponse(responseData:[[String:Any]]?)-> [Attachments]?
        {
            guard responseData != nil else{return nil}
            var userArray:[Attachments] = []
            for dataDict in responseData ?? [] {
                var userLoginDetails = Attachments()
                userLoginDetails.type = dataDict["type"] as? String
                userLoginDetails.fileName = dataDict["fileName"] as? String
                userLoginDetails.name = dataDict["name"] as? String
                userLoginDetails.number = dataDict["number"] as? String
                userLoginDetails.fileLink =  dataDict["fileLink"] as? String
                userArray.append(userLoginDetails)
                
            }
            return userArray
        }
    }
    
    struct Comments:Hashable {
        var commentDescription:String?
        var dateTime:String?
        
      
        
        static func decodeResponse(responseData:[[String:Any]]?)-> [Comments]?
        {
            guard responseData != nil else{return nil}
            var userArray:[Comments] = []
            for dataDict in responseData ?? [] {
                var userLoginDetails = Comments()
                userLoginDetails.commentDescription = dataDict["commentDesc"] as? String
                userLoginDetails.dateTime = dataDict["dateTime"] as? String
                userArray.append(userLoginDetails)
                
            }
            return userArray
        }
    }
    
    static func == (lhs: TaskList, rhs: TaskList) -> Bool {
        return lhs.taskId != rhs.taskId
    }
}
