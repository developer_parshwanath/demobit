//
//  Constant.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation



// MARK:- Cell Name

let myTaskTableViewCell = "MyTaskTableViewCell"
let userManagementTableViewCell = "UserMangementTableViewCell"
let commentTableViewCell = "CommentTableViewCell"



// MARK:- ViewController Name

let myTaskViewController = "MyTaskViewController"
let allTaskViewController = "AllTasksViewController"
let assignedTaskViewController = "AssignedTaskViewController"
let notificationViewController = "NotificationViewController"
let createTaskViewController = "CreateTaskViewController"
let taskDetailViewController = "TaskDetailViewController"
let loginViewController = "LoginViewController"
let menuViewController = "MenuViewController"
let attachmentViewController = "AttachmentViewController"
let userManagementViewController = "UserManagementViewController"
let addUserViewController = "AddUserViewController"
let deviceContactListViewController = "DeviceContactListViewController"


// MARK:- Task Status
let assigned = "assigned"
let unassigned = "unassigned"
let completed = "completed"
let closed = "closed"
let overdue = "overdue"
let pending = "pending"


//MARK:- Userdefaults constant

let userName = "username"
let alreadyLogin = "alreadylogin"
