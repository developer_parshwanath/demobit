//
//  Comment.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation


struct FetchCommentList {
    var taskNumber:String?
    var pageSize:Int?
    var pageNumber:Int?
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "taskNumber" :taskNumber ?? "",
            "pageSize":pageSize ?? 0 ,
            "pageNumber":pageNumber ?? 0
        ]
        
    }
}


struct CommentList {
    var createdBy:String?
    var userName:String?
    var commentDesc:String?
    var dateTime:String?
    var commentId:String?
   
   
    
    static func decodeResponse(responseData:[String:Any]?)-> [CommentList]?
    {
        guard responseData != nil else{return nil}
        guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
        var userArray:[CommentList] = []
        for dataDict in userDetailArray {
            var userLoginDetails = CommentList()
            userLoginDetails.createdBy = dataDict["createdBy"] as? String
            userLoginDetails.userName = dataDict["username"] as? String
            userLoginDetails.commentDesc = dataDict["commentDesc"] as? String
            userLoginDetails.dateTime = dataDict["dateTime"] as? String
            userLoginDetails.commentId =  dataDict["commentId"] as? String
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
}

struct AddComment {
    var taskNumber:String?
    var userName:String?
    var commentDesc:String?
   
  
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "taskNumber" :taskNumber ?? "",
            "username":userName ?? "" ,
            "comment":commentDesc ?? ""
        ]
        
    }
}


struct EditComment {
    var commentId:String?
    var comment:String?
    
   
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "commentId" :commentId ?? "",
            "comment":comment ?? "" 
           
        ]
        
    }
}


struct DeleteComment {
    var commentId:String?
   
    
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "commentId" :commentId ?? ""
            
        ]
        
    }
}
