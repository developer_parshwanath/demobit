//
//  AttachCollectionViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 7/9/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class AttachCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var attachmentImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.fadedShadow()
        self.contentView.roundedCornersWithBorder()
    }

    func setUI(type:String) -> UICollectionViewCell {
        switch type {
        case "image":
            self.attachmentImageView.image = UIImage.init(named: "Gallery")
            break
        case "audio":
                   self.attachmentImageView.image = UIImage.init(named: "Audio")
                   break
        case "video":
                   self.attachmentImageView.image = UIImage.init(named: "Video")
                   break
        case "contact":
                   self.attachmentImageView.image = UIImage.init(named: "Contact")
                   break
        case "file":
                   self.attachmentImageView.image = UIImage.init(named: "Documents")
                   break
        default:
            self.attachmentImageView.image = UIImage.init(named: "Attachment")
            break
        }
        return self
    }
    
}
