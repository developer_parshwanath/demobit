//
//  ProfileShowService.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

class ProfileShowService
{
   
    
    static func fetchUserProfile(loginUser:FetchProfile,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .get, queryParameters: nil,reqBody: loginUser.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            LoadingIndicatorView.hide()
            if(status)
            {
                completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
    }
    
    static func editUserProfile(loginUser:EditProfile,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: loginUser.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            LoadingIndicatorView.hide()
            if(status)
            {
                completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
    }
    
}


//MARK:- User Service
class UserService
{
    
    
    static func  addUser(userDetails:SaveUser,entity:Entity,completion:@escaping(_ responseMode:[[String]]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: userDetails.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            LoadingIndicatorView.hide()
            if(status)
            {
                guard let responseData = dataDict?["data"] as? [[String]] else {
                    completion(nil,dataDict?["message"] as! String,status)
                    return
                }
                completion(responseData,dataDict?["message"] as! String,status)
                
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
    }
    
    
    static func editUser(userDetails:EditUser,entity:Entity,completion:@escaping(_ responseMode:[[String]]?,_ message:String,_ status:Bool)-> Void){
         LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: userDetails.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
        LoadingIndicatorView.hide()
        if(status)
        {
            guard let responseData = dataDict?["data"] as? [[String]] else {
                completion( nil,dataDict?["message"] as? String ?? "",status)
                return
            }
            completion(responseData,dataDict?["message"] as! String,status)
            
            return
        }else
        {
            completion( nil,dataDict?["message"] as? String ?? "",status)
        }
    }
    
    }
    
    static func deleteUser(userDetails:DeleteUser,entity:Entity,completion:@escaping(_ responseMode:[[String]]?,_ message:String,_ status:Bool)-> Void){
         LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: userDetails.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
        LoadingIndicatorView.hide()
        if(status)
        {
            guard let responseData = dataDict?["data"] as? [[String]] else {
                completion( nil,dataDict?["message"] as? String ?? "",status)
                return
            }
            completion(responseData,dataDict?["message"] as! String,status)
            
            return
        }else
        {
           completion( nil,dataDict?["message"] as? String ?? "",status)
        }
    }
    
    }
    
}
