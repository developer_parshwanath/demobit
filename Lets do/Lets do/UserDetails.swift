//
//  UserDetails.swift
//  Lets do
//
//  Created by Darshan Bagmar on 18/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct UserDetails {
    var userName:String?
    var password:String?
    var deviceType:String?
    var isValidUser:Bool = false
    
    func encodeRequest() -> [String:Any]
    {
        ["username":userName ?? "",
         "password":password ?? "",
         "deviceType" : deviceType ?? ""
        ]
    }
    
    func validateUserName() -> String? {
        
        return self.userName == nil || (self.userName?.isEmpty ?? true) || (!Validation.isValidEmail(EmailId: (self.userName ?? ""))) ? "Please enter valid user name" : nil
    }
    
    func validateLoginCredentials() -> String? {
        
        if(self.validateUserName() != nil)
        {
            return self.validateUserName()
            
        }else if(self.password == nil || self.password?.isEmpty ?? true)
        {
            return "Please enter valid password"
        }
        
        return nil
    }
    
    
}
