//
//  MenuProfileHeaderViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 6/29/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class MenuProfileHeaderViewCell: UITableViewHeaderFooterView {

    
    @IBOutlet weak var nameLable:UILabel!
    @IBOutlet weak var outerView:UIView!
    @IBOutlet weak var profilePicView:UIImageView!
    static let reuseIdentifier: String = String(describing: self)
    
    static var nib: UINib {
          return UINib(nibName: String(describing: self), bundle: nil)
      }
    
    func setUPView(name:String,imageURL:String,userLogin:UserLogin)  {
        self.nameLable.text = userLogin.name ?? " "
        self.profilePicView.image = UIImage(named: "MyProfile")
        self.outerView.layer.cornerRadius = self.outerView.bounds.height/2
         self.profilePicView.layer.cornerRadius = self.profilePicView.bounds.height/2
    }

   
}
