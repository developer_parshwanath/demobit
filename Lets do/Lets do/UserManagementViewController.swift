//
//  UserManagementViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 07/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class UserManagementViewController: UIViewController {

    let loggedInUser = LoginService.getLoginUser()
    @IBOutlet weak var userTableView: UITableView!
    var users:[Users] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpMyTaskCell()
        self.getUserList()
        // Do any additional setup after loading the view.
    }


    @IBAction func onBackButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addUserButtonClick(_ sender: Any) {
        let adduserVC:AddUserViewController = AddUserViewController(nibName: addUserViewController, bundle: nil)
        adduserVC.entityType = "admin/addUser"
        self.navigationController?.pushViewController(adduserVC, animated: true)
    }
    
    func getUserList(){
        let fetchUser = FetchUserList.init(orgId: loggedInUser.orgId, pageSize: 10, pageNumber: 1)
        UserServices.getUserList(userInfo: fetchUser, entity: "admin/users", completion: { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                self.users = data ?? []
                self.userTableView.reloadData()
                self.displayToast(message:message)
            }
        })
        

    }
    
}


extension UserManagementViewController:UITableViewDataSource,UITableViewDelegate{
    
    func setUpMyTaskCell()  {
        self.userTableView.delegate = self
        self.userTableView.dataSource = self
        
        self.userTableView.register(UINib(nibName: userManagementTableViewCell, bundle: nil), forCellReuseIdentifier: userManagementTableViewCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:UserMangementTableViewCell = tableView.dequeueReusableCell(withIdentifier: userManagementTableViewCell, for: indexPath) as? UserMangementTableViewCell else {
            
            return UITableViewCell(frame:CGRect.zero)
        }
        cell.setUpData(user: self.users[indexPath.row])
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let adduserVC:AddUserViewController = AddUserViewController(nibName: addUserViewController, bundle: nil)
               adduserVC.entityType = "admin/editUser"
        adduserVC.userDetail = self.users[indexPath.row]
        self.navigationController?.pushViewController(adduserVC, animated: true)
    }
}
