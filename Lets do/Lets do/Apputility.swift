//
//  Apputility.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//
import UIKit
import Foundation


enum Validation
{
    //email validation
       static func isValidEmail(EmailId:String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
           
           let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailTest.evaluate(with: EmailId)
       }
       //mobile number validation
       static func isValidMobileNumber(MobileNumber:String) -> Bool {
           let MoNoRegex = "^[5-9]\\d{0}+[0-9]\\d{8}"
           let MoNoTest = NSPredicate(format:"SELF MATCHES %@", MoNoRegex)
           return MoNoTest.evaluate(with: MobileNumber)
       }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UITextField
{
    
    
    func setUpRightIcon(image:UIImage?)  {
        
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height)*0.90), height: (self.frame.height)*1.0))
        btnView.setImage(image, for: UIControl.State.normal)
        //btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
      //  btnView.backgroundColor = UIColor.red
        self.rightView = btnView
        self.rightViewMode = .always
       
        
        
    }
    
    func setUpRightName(name:String,selectedName:String?,color:UIColor,selectedColor:UIColor?)  {
        
    
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height)*0.90), height: (self.frame.height)*1.0))
        btnView.setTitle(name, for: UIControl.State.normal)
        btnView.setTitle(selectedName, for: UIControl.State.selected)
        
        btnView.setTitleColor(color, for: UIControl.State.normal)
        btnView.setTitleColor(selectedColor, for: UIControl.State.selected)
        
        btnView.titleLabel?.font = self.font
        
        self.rightView = btnView
        self.rightViewMode = .always
       
        
        
    }
    
    func setUpLeftIcon(image:UIImage?)  {
      
          let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height)*0.90), height: (self.frame.height)*1.0))
          btnView.setImage(image, for: UIControl.State.normal)
          self.leftView = btnView
          self.leftViewMode = .always
        self.bounds = CGRect(origin: CGPoint(x: self.bounds.origin.x + 8, y: self.bounds.origin.y), size: self.bounds.size)
         
      }
    
    
     
    
}

extension UIViewController
{
    func displayToast(message:String)
    {
        self.view.makeToast(message)
    }
}


extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}


extension Date {
    
    func localizedDescription(dateStyle: DateFormatter.Style = .medium,
                                 timeStyle: DateFormatter.Style = .medium,
                              in timeZone : TimeZone = .current,
                                 locale   : Locale = .current) -> String {
           Formatter.date.locale = locale
           Formatter.date.timeZone = timeZone
           Formatter.date.dateStyle = dateStyle
           Formatter.date.timeStyle = timeStyle
           return Formatter.date.string(from: self)
       }
    
    func currentTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
       var localizedDescription: String { localizedDescription() }

    var fullDate: String   { localizedDescription(dateStyle: .full,   timeStyle: .none) }
    var longDate: String   { localizedDescription(dateStyle: .long,   timeStyle: .none) }
    var mediumDate: String { localizedDescription(dateStyle: .medium, timeStyle: .none) }
    var shortDate: String  { localizedDescription(dateStyle: .short,  timeStyle: .none) }

    var fullTime: String   { localizedDescription(dateStyle: .none,   timeStyle: .full) }
    var longTime: String   { localizedDescription(dateStyle: .none,   timeStyle: .long) }
    var mediumTime: String { localizedDescription(dateStyle: .none,   timeStyle: .medium) }
    var shortTime: String  { localizedDescription(dateStyle: .none,   timeStyle: .short) }

    var fullDateTime: String   { localizedDescription(dateStyle: .full,   timeStyle: .full) }
    var longDateTime: String   { localizedDescription(dateStyle: .long,   timeStyle: .long) }
    var mediumDateTime: String { localizedDescription(dateStyle: .medium, timeStyle: .medium) }
    var shortDateTime: String  { localizedDescription(dateStyle: .short,  timeStyle: .short) }
}


extension TimeZone {
    static let gmt = TimeZone(secondsFromGMT: 0)!
}
extension Formatter {
    static let date = DateFormatter()
}


extension UIView{
    
    func removeChildView(){
           self.subviews.forEach { $0.removeFromSuperview() }
       }
       
       
          
       func showActivityIndicatory() {
              let view = UIView.init()
              view.frame = self.bounds
              view.backgroundColor = .white
              view.layer.cornerRadius = 10
              let activityIndicator = UIActivityIndicatorView()
              activityIndicator.hidesWhenStopped = true
              if #available(iOSApplicationExtension 13.0, *) {
                  if #available(iOS 13.0, *) {
                      activityIndicator.style = UIActivityIndicatorView.Style.large
                  } else {
                      activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
                      // Fallback on earlier versions
                  }
              } else {
                  activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
                  // Fallback on earlier versions
              }
              activityIndicator.color = .blue
              activityIndicator.translatesAutoresizingMaskIntoConstraints = false
              activityIndicator.transform = CGAffineTransform(scaleX: 2, y: 2)
              activityIndicator.startAnimating()
              view.addSubview(activityIndicator)
            //  printLog("view frame:\(view.frame)....view bounds\(view.bounds)")
              let label = UILabel(frame: CGRect(x: self.bounds.size.width/2, y: 0, width: 200, height: 21))
                label.center = CGPoint(x: (self.bounds.size.width/2), y:self.bounds.size.height/2)
              label.textAlignment = .center
              label.text = "Procesando..."
              label.font = UIFont(name: "Helvetica-Bold", size: 25.0)
              label.textColor = .darkGray
              self.fadeViewInThenOut(view: label, delay: 0)
              view.addSubview(label)
              
              self.addSubview(view)
              activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
              activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
              
                   
             }
       
       func fadeViewInThenOut(view : UIView, delay: TimeInterval) {
           let animationDuration = 0.5
           UIView.animate(withDuration: animationDuration, delay: delay, options: [UIView.AnimationOptions.autoreverse, UIView.AnimationOptions.repeat], animations: {
               view.alpha = 0.75
           }, completion: nil)
       }
}




extension Sequence where Iterator.Element: Hashable {
func unique() -> [Iterator.Element] {
    var seen: Set<Iterator.Element> = []
    return filter { seen.insert($0).inserted }
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}
