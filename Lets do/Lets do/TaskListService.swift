//
//  TaskListService.swift
//  Lets do
//
//  Created by Darshan Bagmar on 18/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

class TaskListService{
    
    static func taskList(taskLists:TaskLists,entity:Entity,completion:@escaping(_ responseMode:[TaskList]?,_ message:String,_ status:Bool)-> Void){
        
        //        let userDetails:UserDetails = UserDetails(userName: "dipalikakade@gmail.com", password: "", isValidUser: false)
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .get, queryParameters: nil,reqBody: taskLists.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            LoadingIndicatorView.hide()
            if(status)
            {
                completion(TaskList.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
        
        
    }
    
}
