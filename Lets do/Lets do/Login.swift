//
//  Login.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct UserLogin {
    var name:String?
    var role:String?
    var mobileNumber:String?
    var emailId:String?
    var userId:String?
    var orgId:String?
    var profilePic:String?
    var token:String?
  //  var deviceType:String?
   
    static func decodeResponse(responseData:[String:Any]?)-> [UserLogin]?
    {
        guard responseData != nil else{return nil}
        guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
        var userArray:[UserLogin] = []
        for dataDict in userDetailArray {
            var userLoginDetails = UserLogin()
            userLoginDetails.name = dataDict["name"] as? String
            userLoginDetails.role = dataDict["role"] as? String
            userLoginDetails.mobileNumber = dataDict["mobileNo"] as? String
            userLoginDetails.emailId = dataDict["email"] as? String
            userLoginDetails.userId =  dataDict["userId"] as? String
            userLoginDetails.token  = dataDict["authunticationTokan"] as? String
            userLoginDetails.orgId = dataDict["orgId"] as? String
            userLoginDetails.profilePic = dataDict["profilePic"] as? String
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
}
