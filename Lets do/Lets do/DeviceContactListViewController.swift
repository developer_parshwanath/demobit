//
//  DeviceContactListViewController.swift
//  GreaseMonkey
//
//  Created by Darshan Bagmar on 9/11/19.
//  Copyright © 2019 Dabstech. All rights reserved.
//

import UIKit
import Contacts

enum AttachMentTypes
{
    case camera , gallary , doucment , audio , contact , video , none
    
    
}


protocol Attachment {
    var type:AttachMentTypes { get set }
}

struct DeviceContact:Attachment {
   
    var type: AttachMentTypes
    var name:String?
    var number:String?
    var isSelected:Bool = false
}

class DeviceContactListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var contactStore = CNContactStore()
    var contactList:[DeviceContact] = [];
    
    var taskAttachment:TaskAttachment?
    
    
    @IBOutlet weak var contactTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.registerTableCell()
        
        self.requestForAccess { (status) in
            print(status)
            
            if(status)
            {
                self.retrieveContactsWithStore(store: self.contactStore)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
            // Do any additional setup after loading the view.
        }
        
    }
    
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func OkAction(_ sender: UIButton) {
       
          let selectedContactList = self.contactList.filter({$0.isSelected == true})
        print(selectedContactList)
        self.taskAttachment?.attach(attachments: selectedContactList)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        
                        DispatchQueue.main.async {
                            let message = "\(accessError?.localizedDescription ?? "")\n\nThis app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?."
                            self.showMessage(message: message)
                        }
                        
                        
                        
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
    
    func retrieveContactsWithStore(store: CNContactStore) {
        do {
            let groups = try store.groups(matching: nil)
            let predicate = CNContact.predicateForContactsInGroup(withIdentifier: groups[0].identifier)
            //let predicate = CNContact.predicateForContactsMatchingName("John")
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactEmailAddressesKey,CNContactPhoneNumbersKey] as [Any]
            
            let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
            print(contacts)
            
            let singleContact = contacts[0]
            let phoneNumber =  singleContact.phoneNumbers.first
            
            for singleContact in contacts {
                
                var deviceContact:DeviceContact = DeviceContact(type: AttachMentTypes.contact)
                
                deviceContact.name = singleContact.givenName
                deviceContact.number = singleContact.phoneNumbers.first?.value.stringValue ?? ""
                self.contactList.append(deviceContact)
                
            }
            
            
           
            
            print(phoneNumber?.value.stringValue ?? "");
            
            self.contactTableView.reloadData();
            
        } catch {
            print(error)
        }
    }
    
    func showMessage(message: String) {
        let alertController = UIAlertController(title: "Contact", message: message, preferredStyle: UIAlertController.Style.alert)
        
        let openSettingsAction = UIAlertAction(title: "Open Settings", style: UIAlertAction.Style.default) { (action) -> Void in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { (action) -> Void in
            
        }
        
        alertController.addAction(openSettingsAction)
        alertController.addAction(dismissAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Table View Delegates
    
    func registerTableCell()  {
        
        self.contactTableView.delegate = self
        self.contactTableView.dataSource = self
        
        self.contactTableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        
        guard let cell:ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as? ContactTableViewCell else
        {
            return UITableViewCell(frame: CGRect.zero)
        }
        
        
        //  let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
        let contact:DeviceContact = self.contactList[indexPath.row]
        
        cell.nameLabel.text = contact.name
        cell.contactNumberlabel.text =  contact.number
        
       if(contact.isSelected)
        {
            cell.selectbtn.image = UIImage(named: "CheckBox")
         
        }else{
            
         cell.selectbtn.image = nil
        }
        
       // cell.selectbtn.tag = indexPath.row
       // cell.selectbtn.addTarget(self, action: #selector(onSelectContact), for: UIControl.Event.touchUpInside)
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var deviceContact = self.contactList[indexPath.row]
        deviceContact.isSelected = !deviceContact.isSelected
        
        self.contactList[indexPath.row] = deviceContact
        self.contactTableView.reloadData()
    }

    @objc func onSelectContact(sender: UIButton)  {
        
        var deviceContact = self.contactList[sender.tag]
        deviceContact.isSelected = !deviceContact.isSelected
        
        self.contactList[sender.tag] = deviceContact
        self.contactTableView.reloadData()
        
     
    }
    
    
}
