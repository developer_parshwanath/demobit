//
//  CommentViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 11/09/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

protocol commentOperation{
    func editCommentClick(comment:CommentList)
    func deleteCommentClick(comment:CommentList)
}

class CommentViewController: UIViewController {

    @IBOutlet weak var commentViewTableView: UITableView!
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var enterCommentTextField: UITextField!
    var delegate:commentOperation?
    var commentID:String?
    var userInfo:UserLogin?
    var arrayOfComments:[CommentList]?
    var taskDetails:TaskView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpCommentViewCell()
        self.fetchComment()
        self.setupToolbar()
        let yourColor : UIColor = UIColor( red: 0.7, green: 0.3, blue:0.1, alpha: 1.0 )
        commentView.layer.masksToBounds = true
        commentView.layer.borderColor = yourColor.cgColor
        commentView.layer.borderWidth = 1.0
        
        
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func setupToolbar(){
       //Create a toolbar
       let bar = UIToolbar()
       //Create a done button with an action to trigger our function to dismiss the keyboard
       let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissMyKeyboard))
       //Create a felxible space item so that we can add it around in toolbar to position our done button
       let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
       //Add the created button items in the toobar
       bar.items = [flexSpace, flexSpace, doneBtn]
       bar.sizeToFit()
       //Add the toolbar to our textfield
           self.enterCommentTextField.inputAccessoryView = bar
           
       }
       @objc func dismissMyKeyboard(){
       view.endEditing(true)
       }
    
    func fetchComment() {
        let fetchComment = FetchCommentList.init(taskNumber: taskDetails?.taskNumber, pageSize: 10, pageNumber: 1)
        CommentService.fetchCommentList(fetchComment: fetchComment, entity:"user/comments" ) { (commentList, message,status)  in
            
            if !status || commentList == nil || commentList?.count ?? 0 < 1
                      {
                          self.enterCommentTextField.text = " "
                        self.displayToast(message: message)
                      }else{
                          
                          self.displayToast(message: message)
                          self.arrayOfComments = commentList
                          DispatchQueue.main.async {
                             self.enterCommentTextField.text = " "
                             self.commentViewTableView.reloadData()
                          }
                      }
        }
        
    }
    
    
    func editComment(commentId:String){
        let editComments = EditComment.init(commentId: commentId, comment: enterCommentTextField.text)
        CommentService.editCommentService(editComment: editComments, entity:"user/editComment") { (commentList, message,status)  in
            
            if status
            {
                self.displayToast(message: message)
                self.commentID = nil
                self.fetchComment()
            }else
            {
                self.commentID = nil
                self.displayToast(message:message)
            }
        }
        

    }
    
    func deleteComment(commentId:String){
        let deletComment = DeleteComment.init(commentId: commentId)
        CommentService.deleteCommentService(deleteComment:deletComment , entity:  "user/deleteComment") { (commentList, message, status) in
            
            if status
            {
                self.displayToast(message:message)
                self.commentID = nil
                self.fetchComment()
            }else
            {
                self.displayToast(message: message)
                self.commentID = nil
            }
        }
        
    }
    
    @IBAction func onBackButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func commentSubmitBtnClick(_ sender: Any) {
        if enterCommentTextField.text != nil && enterCommentTextField.text != " " {
            if !(commentID != nil && commentID != " ") {
                let addComents = AddComment.init(taskNumber: taskDetails?.taskNumber, userName: userInfo?.emailId, commentDesc: enterCommentTextField.text)
                CommentService.addCommentService(addComment: addComents, entity:"user/addComment"  ) { (commentList, message,status) in
                    
                    if status
                    {
                        self.displayToast(message: message)
                        self.fetchComment()
                    }else
                    {
                        self.displayToast(message: message)
                        
                    }
                }
                
            }else{
                self.editComment(commentId: commentID ?? " ")
            }
            
        }else{
            self.displayToast(message: "Please enter comment")
        }
    }
}


extension CommentViewController:UITableViewDelegate,UITableViewDataSource{
    
    func setUpCommentViewCell()  {
        self.commentViewTableView.delegate = self
        self.commentViewTableView.dataSource = self
        
        self.commentViewTableView.register(UINib(nibName: commentTableViewCell, bundle: nil), forCellReuseIdentifier: commentTableViewCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfComments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:CommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: commentTableViewCell, for: indexPath) as? CommentTableViewCell else {
            
            return UITableViewCell(frame:CGRect.zero)
        }
        _ = cell.setData(comment: (self.arrayOfComments?[indexPath.row])!)
        cell.delegate = self
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}

extension CommentViewController:commentOperation{
    func editCommentClick(comment: CommentList) {
        self.commentID = comment.commentId
        self.enterCommentTextField.text = comment.commentDesc
        self.enterCommentTextField.becomeFirstResponder()
    }
    
    func deleteCommentClick(comment: CommentList) {
        self.deleteComment(commentId: comment.commentId ?? " ")
    }
}
