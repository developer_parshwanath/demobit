//
//  AllTasksViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class AllTasksViewController: UIViewController {
    var allTask:[TaskList]?
    @IBOutlet weak var allTaskTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpAllTaskCell()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       // let date = dateFormatter.date(from: strDate)
       
        self.allTask = Array(Set(self.allTask!))
        
        
        
        self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
            return dateFormatter.date(from: taskOne.createdDate!)!.compare(dateFormatter.date(from: taskTwo.createdDate!)!) == ComparisonResult.orderedDescending
          //  return taskOne.createdDate > task
        }
        // Do any additional setup after loading the view.
    }

   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onSortButtonClick(_ sender: Any) {
        
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .actionSheet)
            let actionDueDate = UIAlertAction(title: "Due Date", style: .default) {
                UIAlertAction in
                // Write your code here
                let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (dateFormatter.date(from: taskOne.dueDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.dueDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                    }
                DispatchQueue.main.async {
                    self.allTaskTableView.reloadData()
                }
            }
            alert.addAction(actionDueDate)
            
            let actionPriority = UIAlertAction(title: "Priority", style: .default) {
                       UIAlertAction in
                       // Write your code here
                        self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (taskOne.priority ?? " ").compare(taskTwo.priority ?? " ") == ComparisonResult.orderedDescending
                        }
                    DispatchQueue.main.async {
                        self.allTaskTableView.reloadData()
                    }
                   }
                   alert.addAction(actionPriority)
            
            let actionCOD = UIAlertAction(title: "Completed On Date", style: .default) {
                       UIAlertAction in
                       // Write your code here
                    let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (dateFormatter.date(from: taskOne.completionDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.completionDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                        }
                    DispatchQueue.main.async {
                        self.allTaskTableView.reloadData()
                    }
                
                   }
                   alert.addAction(actionCOD)
            
            let actionCD = UIAlertAction(title: "Create Date", style: .default) {
                       UIAlertAction in
                       let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (dateFormatter.date(from: taskOne.createdDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.createdDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                        }
                DispatchQueue.main.async {
                    self.allTaskTableView.reloadData()
                }
                    
                   }
                   alert.addAction(actionCD)
            
            let actionCB = UIAlertAction(title: "Created By", style: .default) {
                       UIAlertAction in
                       // Write your code here
                    self.allTask = self.allTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (taskOne.createdBy ?? " ").compare(taskTwo.createdBy ?? " ") == ComparisonResult.orderedDescending
                        }
                   DispatchQueue.main.async {
                        self.allTaskTableView.reloadData()
                    }
                   }
                   alert.addAction(actionCB)

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
                // It will dismiss action sheet
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        
    }
}


extension AllTasksViewController:UITableViewDelegate,UITableViewDataSource{
    
    func setUpAllTaskCell()  {
        self.allTaskTableView.delegate = self
        self.allTaskTableView.dataSource = self
        
        self.allTaskTableView.register(UINib(nibName: myTaskTableViewCell, bundle: nil), forCellReuseIdentifier: myTaskTableViewCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allTask?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:MyTaskTableViewCell = tableView.dequeueReusableCell(withIdentifier: myTaskTableViewCell, for: indexPath) as? MyTaskTableViewCell else {
            
            return UITableViewCell(frame:CGRect.zero)
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
         _ = cell.setData(dictofData: (self.allTask?[indexPath.row])!)
        
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let taskDetailVC:TaskDetailViewController = TaskDetailViewController(nibName: taskDetailViewController, bundle: nil)
        self.navigationController?.pushViewController(taskDetailVC, animated: true)
    }
    
}


extension Array {

    func uniques<T: Hashable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return reduce([]) { result, element in
            let alreadyExists = (result.contains(where: { $0[keyPath: keyPath] == element[keyPath: keyPath] }))
            return alreadyExists ? result : result + [element]
        }
    }
}
