//
//  AssignedTaskViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class AssignedTaskViewController: UIViewController {

    @IBOutlet weak var assignTaskTableView: UITableView!
    @IBOutlet weak var pendingTaskButton: UIButton!
    @IBOutlet weak var completedTaskButton: UIButton!
    @IBOutlet weak var connectedContact: UIButton!
    var delegate:navigationSet?
    var assignedTask:[TaskList]?
    var connectedContactTask:[TaskList]?
    var showAssignedTask:[TaskList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpMyTaskCell()
        self.showAssignedTask = assignedTask?.filter{$0.status == "pending" }
        self.assignTaskTableView.reloadData()
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onPendingTaskButtonClick(_ sender: Any) {
        
        pendingTaskButton.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
        pendingTaskButton.setImage(UIImage.init(named: radioButtonSelected), for: .normal)
       
        completedTaskButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        completedTaskButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        connectedContact.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        connectedContact.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        showAssignedTask = assignedTask?.filter{$0.status == "pending" }
        assignTaskTableView.reloadData()

    }
    
    @IBAction func onCompletedTaskButtonClick(_ sender: Any) {
   
        completedTaskButton.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
        completedTaskButton.setImage(UIImage.init(named: radioButtonSelected), for: .normal)
       
        pendingTaskButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        pendingTaskButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        connectedContact.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        connectedContact.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        showAssignedTask = assignedTask?.filter{$0.status == "completed" }
        assignTaskTableView.reloadData()
    }
    
    
    @IBAction func onConnectedContactClick(_ sender: Any) {
    
        completedTaskButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        completedTaskButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        pendingTaskButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
        pendingTaskButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
         
        connectedContact.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
        connectedContact.setImage(UIImage.init(named: radioButtonSelected), for: .normal)
          
      showAssignedTask = connectedContactTask//assignedTask?.filter{$0.status == "completed" }
      assignTaskTableView.reloadData()
    
        
    
    }
    
    @IBAction func onSortButtonClick(_ sender: Any) {
   
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .actionSheet)
            let actionDueDate = UIAlertAction(title: "Due Date", style: .default) {
                UIAlertAction in
                // Write your code here
                let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    self.showAssignedTask = self.showAssignedTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (dateFormatter.date(from: taskOne.dueDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.dueDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                    }
                DispatchQueue.main.async {
                    self.assignTaskTableView.reloadData()
                }
            }
            alert.addAction(actionDueDate)
            
            let actionPriority = UIAlertAction(title: "Priority", style: .default) {
                       UIAlertAction in
                       // Write your code here
                        self.showAssignedTask = self.showAssignedTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (taskOne.priority ?? " ").compare(taskTwo.priority ?? " ") == ComparisonResult.orderedDescending
                        }
                    DispatchQueue.main.async {
                        self.assignTaskTableView.reloadData()
                    }
                   }
                   alert.addAction(actionPriority)
            
            let actionCOD = UIAlertAction(title: "Completed On Date", style: .default) {
                       UIAlertAction in
                       // Write your code here
                    let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self.showAssignedTask = self.showAssignedTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (dateFormatter.date(from: taskOne.completionDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.completionDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                        }
                    DispatchQueue.main.async {
                        self.assignTaskTableView.reloadData()
                    }
                
                   }
                   alert.addAction(actionCOD)
            
            let actionCD = UIAlertAction(title: "Create Date", style: .default) {
                       UIAlertAction in
                       let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self.showAssignedTask = self.showAssignedTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (dateFormatter.date(from: taskOne.createdDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.createdDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                        }
                DispatchQueue.main.async {
                    self.assignTaskTableView.reloadData()
                }
                    
                   }
                   alert.addAction(actionCD)
            
            let actionCB = UIAlertAction(title: "Created By", style: .default) {
                       UIAlertAction in
                       // Write your code here
                    self.showAssignedTask = self.showAssignedTask?.sorted{(taskOne, taskTwo) -> Bool in
                            return (taskOne.createdBy ?? " ").compare(taskTwo.createdBy ?? " ") == ComparisonResult.orderedDescending
                        }
                   DispatchQueue.main.async {
                        self.assignTaskTableView.reloadData()
                    }
                   }
                   alert.addAction(actionCB)

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
                // It will dismiss action sheet
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        
    }
    
}



extension AssignedTaskViewController:UITableViewDataSource,UITableViewDelegate{
    func setUpMyTaskCell()  {
        self.assignTaskTableView.delegate = self
        self.assignTaskTableView.dataSource = self
        
        self.assignTaskTableView.register(UINib(nibName: myTaskTableViewCell, bundle: nil), forCellReuseIdentifier: myTaskTableViewCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.showAssignedTask?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:MyTaskTableViewCell = tableView.dequeueReusableCell(withIdentifier: myTaskTableViewCell, for: indexPath) as? MyTaskTableViewCell else {
            
            return UITableViewCell(frame:CGRect.zero)
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.myTask = false
         _ = cell.setData(dictofData: (self.showAssignedTask?[indexPath.row])!)
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           self.navigationController?.popViewController(animated: true)
           self.delegate?.vcPopup(taskList: (self.showAssignedTask?[indexPath.row])!)
       }
}
