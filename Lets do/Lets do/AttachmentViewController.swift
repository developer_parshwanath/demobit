//
//  AttachmentViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 07/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit




class AttachmentViewController: UIViewController {

    @IBOutlet weak var buttonView: UIView!
    var delegateCb:TaskAttachment?
    var presentingVc:UIViewController?
    var presentingCreateVc:CreateTaskViewController?
    var presentingTaskVC:TaskDetailViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        if presentingTaskVC != nil {
            presentingVc = presentingTaskVC
        }else if presentingCreateVc != nil {
            presentingVc = presentingCreateVc
        }
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         let touch = touches.first
         if touch?.view != self.buttonView {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onCameraButtonClick(_ sender: Any) {
    
        self.dismiss(animated: true) {
    
            }
        
       AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType.camera, vc: self.presentingVc!)
        
        
        AttachmentHandler.shared.imagePickedBlock = {(image,url) in

                  self.delegateCb?.addFile(fileType: "image", fileUrl: url, image: image)
                  self.dismiss(animated: true, completion: nil)
                }
        
        
    }
    
    @IBAction func onGalleryButtonClick(_ sender: Any) {
        self.dismiss(animated: true) {
                          
                                  }
        
        AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType.photoLibrary, vc: self.presentingVc!)
               
        AttachmentHandler.shared.imagePickedBlock = {(image,url) in
            print("image url :\(url)")
            self.delegateCb?.addFile(fileType: "image", fileUrl: url, image: image)
            self.dismiss(animated: true, completion: nil)
            }
        
       
    }
    
    @IBAction func onDocumentButtonClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
                   
                           }
        
       AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType.file, vc: self.presentingVc!)
                      
               AttachmentHandler.shared.filePickedBlock = {(url) in
                   
                   }
    }
    
   
    @IBAction func onAudioButtonClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
                   
            }
        AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType.video, vc: self.presentingVc!)
               
        AttachmentHandler.shared.filePickedBlock = {(url) in
            self.delegateCb?.addFile(fileType: "audio", fileUrl: url as NSURL, image: UIImage.init())
            self.dismiss(animated: true, completion: nil)
           
        }
        
    }
    
    
    @IBAction func onContactButtonClick(_ sender: Any) {
    
        
        self.dismiss(animated: true) {
               let contactListVc:DeviceContactListViewController = DeviceContactListViewController(nibName: deviceContactListViewController, bundle: nil)
             contactListVc.modalPresentationStyle = .overCurrentContext
            contactListVc.taskAttachment = self.presentingVc as! TaskAttachment
            
            self.presentingVc?.present(contactListVc, animated: true, completion: nil)
           // self.presentingVc?.navigationController?.pushViewController(contactListVc, animated: true)
        }
        
        
    }
    
    
    
    @IBAction func onVideoButtonClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
               
        }
        
      AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType.video, vc: self.presentingVc!)
        
                             
                      AttachmentHandler.shared.videoPickedBlock = {(url) in
                        self.delegateCb?.addFile(fileType: "video", fileUrl: url, image: UIImage.init())
                          self.dismiss(animated: true, completion: nil)
                          
                    }
    
    }
    
    
   
}

