//
//  Profile.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct UserProfile{
    var name: String?
    var role: String?
    var mobileNo, email: String?
    var userId: String?
    var orgToken, profilePic: String?
    var token:String?

    
    
    static func decodeResponse(responseData:[String:Any]?)-> [UserProfile]?
       {
           guard responseData != nil else{return nil}
           guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
           var userArray:[UserProfile] = []
           for dataDict in userDetailArray {
               var userLoginDetails = UserProfile()
               userLoginDetails.name = dataDict["name"] as? String
               userLoginDetails.role = dataDict["role"] as? String
               userLoginDetails.mobileNo = dataDict["mobileNo"] as? String
               userLoginDetails.email = dataDict["email"] as? String
               userLoginDetails.userId =  dataDict["userId"] as? String
               userLoginDetails.token  = dataDict["authunticationToka"] as? String
               userLoginDetails.orgToken = dataDict["orgId"] as? String
               userLoginDetails.profilePic = dataDict["profilePic"] as? String
                userLoginDetails.token = dataDict["token"] as? String
               userArray.append(userLoginDetails)
               
           }
           return userArray
       }
}

struct FetchProfile
{
    //var token:String
    var username:String
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "username" :username
           
        ]
        
    }
}

struct  EditProfile {
    var token:String
    var username:String
    var name:String
    var mobileNo:String
    var profilePic:String
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "token" :token ,
            "username":userName ,
            "name":name ,
            "mobileNo" :mobileNo ,
            "profilePic":profilePic
        ]
        
    }
}


