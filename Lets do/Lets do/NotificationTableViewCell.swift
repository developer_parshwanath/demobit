//
//  NotificationTableViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 6/27/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var headerlabel:UILabel!
    @IBOutlet weak var detailLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    
    @IBOutlet weak var outerView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outerView.layer.cornerRadius = 8.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpUI()  {
        
        self.headerlabel.text = "NOTIFICATIOn HEADER"
        self.detailLabel.text = "Notification detail text apperas heare"
        self.dateLabel.text = "09 June 2020"
    }
    
}
