//
//  TaskEdit.swift
//  Lets do
//
//  Created by Darshan Bagmar on 09/09/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct TaskEdit{
    var entity:String?
    var orgId:String?
    var userName:String?
    var assignee:String?
    var taskName:String?
    var taskDesc:String?
    var priority:String?
    var status:String?
    var dueDate:String?
    var taskNumber:String?
    var attachments:[Attachments]? // attchments array
    var comments:[Comments]?
    var connectedContacts:[ConnectedContacts]?
    
    func encodeRequest() -> [String:Any]
    {
        return [
            
        "entity":entity ?? "",
        "orgId":orgId ?? "",
        "username":userName ?? "",
        "assignee":assignee ?? "",
        "taskName":taskName ?? "",
        "taskDesc":taskDesc ?? "",
        "priority":priority ?? "",
        "status":status ?? "",
        "dueDate":dueDate ?? "",
        "taskNumber":taskNumber ?? "" ,
        "attachments":attachments?.compactMap({ (attachment) -> [String:Any]? in
           return attachment.encodeRequest()
        }) ?? [],
        "comments":comments?.compactMap({ (comments) -> [String:Any]? in
            return comments.encodeRequest()
        }) ?? [],
        "connectedContacts":connectedContacts?.compactMap({ (connect) -> [String:Any]? in
            return connect.encodeRequest()
        }) ?? [],
        
        ]
        
    }
    
    
}

