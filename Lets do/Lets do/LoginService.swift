//
//  LoginService.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

class LoginService
{
    private static var loginUser:UserLogin = UserLogin()
    
    static func login(userDetails:UserDetails,entity:Entity,completion:@escaping(_ responseModel:[UserLogin]?,_ message:String,_ status:Bool)-> Void){
        
        //        let userDetails:UserDetails = UserDetails(userName: "dipalikakade@gmail.com", password: "", isValidUser: false)
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: userDetails.encodeRequest(),isTockenRequired: true, tockenKey: TEMP_TOKEN, completionHandler: { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion( UserLogin.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        })
        
        
        
        
        
    }
    
    
    
    
}


//MARK:- Token Save
extension LoginService
{
    
    class func getToken(tokenKey:String) -> String? {
        
        guard let token:String = UserDefaults.standard.value(forKey: tokenKey) as? String
            else {
                
                return nil;
        }
        
        return token
    }
    
    
    class func saveNewTockenData(tokenString:String?,tockenKey:String) {
        
        guard let token:String = tokenString
            else {
                
                return;
        }
        
        UserDefaults.standard.set(token, forKey: tockenKey)
        
        UserDefaults.standard.synchronize()
        
    }
    
    class func setValiduser(user:UserDetails)
    {
        guard let userName:String = user.userName
            else {
                
                return;
        }
        
        guard let isValidUser:Bool = user.isValidUser
            else {
                
                return;
        }
        
        
        UserDefaults.standard.set(userName, forKey:USER_NAME )
        UserDefaults.standard.set(isValidUser, forKey: IS_VALID_USER)
        UserDefaults.standard.synchronize()
    }
    
    class func checkIsValiduser() -> Bool
    {
        
        guard let isValidToken:Bool = UserDefaults.standard.value(forKey: IS_VALID_USER) as? Bool
            else {
                
                return false;
        }
        
        return isValidToken
        
        
        
    }
    
    class func getLoginUser() -> UserLogin
    {
        return LoginService.loginUser
    }
    
    class func setLoginUser(loginUser:UserLogin)
    {
        LoginService.loginUser = loginUser
    }
}
