//
//  TaskView.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct TaskView {
    var orgId:String?
    var organization:String?
    var createdBy:String?
    var taskNumber:String?
    var assignee:String?
    var assigneeName:String?
    var priority:String?
    var status:String?
    var taskName:String?
    var taskDesc:String?
    var dueDate:String?
    var completionDate:String?
    var createdDate:String?
    var connectedContactName:String?
    var userName:String?
    var attachments:[Attachments]? // attchments array
    var comments:[Comments]?
  //  var connectedContacts:[ConnectedContacts]?
    
    
    
    static func decodeResponse(responseData:[String:Any]?)-> [TaskView]?
    {
        guard responseData != nil else{return nil}
        guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
        var userArray:[TaskView] = []
        for dataDict in userDetailArray {
            var userLoginDetails = TaskView()
            userLoginDetails.orgId = dataDict["orgId"] as? String
            userLoginDetails.organization = dataDict["organization"] as? String
            userLoginDetails.createdBy = dataDict["createdBy"] as? String
            userLoginDetails.taskNumber = dataDict["taskNumber"] as? String
            userLoginDetails.assignee = dataDict["assignee"] as? String
            userLoginDetails.assigneeName = dataDict["assigneeName"] as? String
            userLoginDetails.priority = dataDict["priority"] as? String
            userLoginDetails.status = dataDict["status"] as? String
            userLoginDetails.taskName = dataDict["taskName"] as? String
            userLoginDetails.taskDesc = dataDict["taskDesc"] as? String
            userLoginDetails.dueDate = dataDict["dueDate"] as? String
            userLoginDetails.completionDate = dataDict["completionDate"] as? String
            userLoginDetails.createdDate = dataDict["createdDate"] as? String
            userLoginDetails.connectedContactName = dataDict["connectedContactName"] as? String
            userLoginDetails.userName = dataDict["username"] as? String
            userLoginDetails.attachments = Attachments.decodeResponse(responseData: dataDict["attachments"] as? [[String : Any]] ?? [])
            
            userLoginDetails.comments = Comments.decodeResponse(responseData: dataDict["comments"] as? [[String : Any]] ?? [])
            
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
}


struct fetchTask {
    var taskNumber:String?
   
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "taskNumber" :taskNumber ?? ""
        ]
        
    }
}
