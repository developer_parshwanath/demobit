//
//  CommentTableViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 23/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    var delegate:commentOperation?
    @IBOutlet weak var commentName: UILabel!
    @IBOutlet weak var commentDetail: UILabel!
    var comment:CommentList?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onEditButtonClick(_ sender: Any) {
        self.delegate?.editCommentClick(comment: comment!)
    }
    
    @IBAction func onDeleteButtonClick(_ sender: Any) {
        self.delegate?.deleteCommentClick(comment: comment!)
    }
    
    func setData(comment:CommentList) -> UITableViewCell {
        self.comment = comment
        self.commentName.text = comment.createdBy ?? " "
        self.commentDetail.text = comment.commentDesc ?? " "
       
        return self
    }
}
