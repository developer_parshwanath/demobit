//
//  TaskDetailViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 04/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import QuickLook

class TaskDetailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var attachmentCollectionView: UICollectionView!
    @IBOutlet var taskDetailView: UIView!
    var taskListDict:TaskList?
    var userLogin:UserLogin?
    @IBOutlet weak var taskNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var createdByTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var taskNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var assigneeNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var connectedContactTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var statusTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var taskCompletitoionDateTextField: SkyFloatingLabelTextField!
     var presentingVc:TaskDetailViewController?
    var attachmentFiles:[Attachments] = []
    @IBOutlet weak var statusButton: UIButton!
    lazy var previewItem = NSURL()
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    var taskViewData:TaskView?
    
    
    var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCornerRadius()
        self.setupToolbar()
        self.setTextFieldDelegate()
        self.setUpCollectionViewCell()
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 100, width: screenWidth, height: screenHeight - 90))
        
        scrollView.addSubview(self.taskDetailView)
        
        scrollView.contentSize = CGSize(width: screenWidth - 5, height: self.taskDetailView.frame.height)
        self.setUpViewHeight()
        self.getTaskView()
        view.addSubview(scrollView)
        
        // Do any additional setup after loading the view.
    }
    
    
    func setTextFieldDelegate(){
        self.taskNameTextField.delegate = self
        self.createdByTextField.delegate = self
        self.taskNumberTextField.delegate = self
        self.assigneeNameTextField.delegate = self
        self.connectedContactTextField.delegate = self
        self.statusTextField.delegate = self
        self.taskCompletitoionDateTextField.delegate = self
        self.taskNameTextField.selectedLineColor = .black
        self.taskNameTextField.title = "task Name"
        self.taskNumberTextField.selectedTitle = "task Name"
    }
    
    
    func setUpViewHeight() {
        
        self.taskDetailView.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: 1100.0)
        
        let height:CGFloat = taskDetailView.bounds.height;
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width - 40, height: height)
        
    }
    
    func setCornerRadius(){
        self.descriptionView.fadedShadow()
        self.descriptionView.roundedCornersWithBorder()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func onBackButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getTaskView(){
        let fetchTaskModel = fetchTask.init(taskNumber: taskListDict?.taskNumber)
        TaskViewService.gettaskView(taskLists: fetchTaskModel, entity: "user/taskView") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                
                self.taskViewData = data?.first
                self.taskNumberTextField.text = self.taskViewData?.taskNumber ?? " "
                self.createdByTextField.text = self.taskViewData?.createdBy ?? " "
                self.taskNameTextField.text = self.taskViewData?.taskName ?? " "
                self.descriptionTextView.text = self.taskViewData?.taskDesc ?? " "
                self.assigneeNameTextField.text = self.taskViewData?.assigneeName ?? " "
                self.connectedContactTextField.text = self.taskViewData?.connectedContactName ?? " "
                self.taskCompletitoionDateTextField.text = self.taskViewData?.dueDate ?? " "
                self.statusTextField.text = self.taskViewData?.status ?? " "
                self.attachmentFiles = self.taskViewData?.attachments ?? []
                DispatchQueue.main.async {
                    let attachment = Attachments.init(type: .none, fileName: "", name: "", number: "", fileLink: "")
                    self.attachmentFiles.insert(attachment, at: 0)
                    self.attachmentCollectionView.reloadData()
                }
                print("\(data)")
            }
        }
        
    }
    
    
    @IBAction func onCommentsButtonClick(_ sender: Any) {
        let commentVC:CommentViewController = CommentViewController(nibName:"CommentViewController", bundle: nil)
        commentVC.taskDetails = taskViewData
        commentVC.userInfo = userLogin
        self.navigationController?.pushViewController(commentVC, animated: true)
        
    }
    
    @IBAction func onUpdateButtonClick(_ sender: Any) {
        let attachment = self.attachmentFiles[0]
               if attachment.type == .none {
                   self.attachmentFiles.remove(at: 0)
               }
        
        let attachmentUpload = Attachments.init(type: "image", fileName: "3/IMG_1596466422685.png", name: nil, number: nil, fileLink: "https://ap-south-1.linodeobjects.com/letsdo/3/IMG_1596466422685.png")
        var attach:[Attachments] = []
        attach.append(attachmentUpload)
        
        let taskEdit = TaskEdit.init(entity: "createTask", orgId:self.taskViewData?.orgId , userName: self.taskViewData?.userName, assignee: self.taskViewData?.assignee, taskName: taskNameTextField.text, taskDesc: descriptionTextView.text, priority: self.taskViewData?.priority, status:self.statusTextField.text , dueDate: taskCompletitoionDateTextField.text, taskNumber: self.taskViewData?.taskNumber, attachments: nil, comments: nil, connectedContacts: nil)
        
        TaskEditService.submitTask(createTask: taskEdit, entity: "user/createTask", completion: { (profileData, message,status) in
            
            if status
            {
                self.displayToast(message: "Task Created")
                self.navigationController?.popViewController(animated: true)
            }else {
               self.displayToast(message: "Task Update fail please try again")
                if self.attachmentFiles.count > 0 {
                    let attachment = self.attachmentFiles[0]
                    if attachment.type != .none {
                         DispatchQueue.main.async {
                                           let attachment = Attachments.init(type: .none, fileName: "", name: "", number: "", fileLink: "")
                                           self.attachmentFiles.insert(attachment, at: 0)
                                           self.attachmentCollectionView.reloadData()
                                       }
                    }
                }else{
                   
                         DispatchQueue.main.async {
                                           let attachment = Attachments.init(type: .none, fileName: "", name: "", number: "", fileLink: "")
                                           self.attachmentFiles.insert(attachment, at: 0)
                                           self.attachmentCollectionView.reloadData()
                                       }
                   
                }
            }
            
        })
    }
    
    func setupToolbar(){
        //Create a toolbar
        let bar = UIToolbar()
        //Create a done button with an action to trigger our function to dismiss the keyboard
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissMyKeyboard))
        //Create a felxible space item so that we can add it around in toolbar to position our done button
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        //Add the created button items in the toobar
        bar.items = [flexSpace, flexSpace, doneBtn]
        bar.sizeToFit()
        //Add the toolbar to our textfield
        self.taskNameTextField.inputAccessoryView = bar
        self.createdByTextField.inputAccessoryView = bar
        self.taskNumberTextField.inputAccessoryView = bar
        self.assigneeNameTextField.inputAccessoryView = bar
        self.connectedContactTextField.inputAccessoryView = bar
        self.statusTextField.inputAccessoryView = bar
        self.taskCompletitoionDateTextField.inputAccessoryView = bar
        
        
    }
    @objc func dismissMyKeyboard(){
        view.endEditing(true)
    }
    
    @objc func deleteAttachment() {
        let alert = UIAlertController(title: "", message: "Delete attachment", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (_) in
            self.attachmentFiles.remove(at: 0)
            DispatchQueue.main.async {
                self.attachmentCollectionView.reloadData()
            }
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension TaskDetailViewController{
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.taskNumberTextField || textField == self.createdByTextField {
            return false
        }else if textField == assigneeNameTextField  {
            let contactVC:ContactListViewController = ContactListViewController(nibName: "ContactListViewController", bundle: nil)
            contactVC.userLogin = userLogin
            contactVC.isMultipleselction = false
            contactVC.callBack = { model in
                guard  let contact = model as? Contact else {
                    return
                }
                print(contact)
            }
            self.present(contactVC, animated: false, completion: nil)
            return false
        }else if textField == connectedContactTextField{
            
            let contactVC:ContactListViewController = ContactListViewController(nibName: "ContactListViewController", bundle: nil)
            contactVC.userLogin = userLogin
            contactVC.isMultipleselction = true
            contactVC.callBack = { model in
                guard  let contact = model as? [Contact] else {
                    return
                }
                print(contact)
                
            }
            return false
        }else if textField == statusTextField{
            //self.showAlertWithThreeButton()
            return false
        }else{
            return true
        }
    }
    
    
    func getPreviewItem(withName name: String) -> NSURL{
        
        //  Code to diplay file from the app bundle
        let file = name.components(separatedBy: ".")
        let path = Bundle.main.path(forResource: file.first!, ofType: file.last!)
        let url = NSURL(fileURLWithPath: path!)
        
        return url
    }
    
    func downloadfile(attachment:Attachments,completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        
        let itemUrl = URL(string: attachment.fileLink ?? " ")
        LoadingIndicatorView.show()
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var fileUpadatedName:String?
        
        let array = attachment.fileName?.split(separator: "/")
        if array?.count ?? 0 > 0 {
            fileUpadatedName = String(array![1]) as String
        }else{
            fileUpadatedName = attachment.fileName ?? " "
        }
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(fileUpadatedName ?? " ")
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            LoadingIndicatorView.hide()
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                     LoadingIndicatorView.hide()
                    completion(true, tempLocation)
                } catch let error as NSError {
                    print(error.localizedDescription)
                     LoadingIndicatorView.hide()
                    completion(false, nil)
                }
            }).resume()
        }
    }
    
    func displayFile(attachment:Attachments){
        self.downloadfile(attachment: attachment, completion: {(success, fileLocationURL) in
                   
                   if success {
                       // Set the preview item to display======
                       self.previewItem = fileLocationURL! as NSURL
                       // Display file
                    DispatchQueue.main.async {
                        let previewController = QLPreviewController()
                        previewController.dataSource = self
                        self.present(previewController, animated: true, completion: nil)
                    }
                   }else{
                       debugPrint("File can't be downloaded")
                   }
               })
    }
    
}

extension TaskDetailViewController:TaskAttachment{
        func addFile(fileType: String, fileUrl: NSURL, image:UIImage) {
            LoadingIndicatorView.show()
            if fileType == "image" {
                //guard let image = image else { return } //1
                AWSS3Manager.shared.uploadImage(image: image, progress: {[weak self] ( uploadProgress) in
                    
                    guard let strongSelf = self else { return }
                  //  strongSelf.progressView.progress = Float(uploadProgress)//2
                    
                }) {[weak self] (uploadedFileUrl, error) in
                    
                    guard let strongSelf = self else { return }
                    if let finalPath = uploadedFileUrl as? String { // 3
                      print("final path \(finalPath)")
                        let imageAttach = Attachments.init(type: fileType, fileName: "test.png", name: "", number: "", fileLink: finalPath)
                        self?.attachmentFiles.append(imageAttach)
                        LoadingIndicatorView.hide()
                        DispatchQueue.main.async {
                            self?.attachmentCollectionView.reloadData()
                             }
                    } else {
                        LoadingIndicatorView.hide()
                        print("\(String(describing: error?.localizedDescription))") // 4
                    }
                }
            }else if fileType == "video"{
                let videoUrl = fileUrl.fileReferenceURL()!
                AWSS3Manager.shared.uploadVideo(videoUrl: videoUrl, progress: { [weak self] (progress) in
                   
                    
                }) { [weak self] (uploadedFileUrl, error) in
                    guard let strongSelf = self else { return }
                    if let finalPath = uploadedFileUrl as? String {
                        print("final path \(finalPath)")
                        let imageAttach = Attachments.init(type: fileType, fileName: "", name: "", number: "", fileLink: finalPath)
                        self?.attachmentFiles.append(imageAttach)
                        LoadingIndicatorView.hide()
                        DispatchQueue.main.async {
                                               self?.attachmentCollectionView.reloadData()
                        }
                    } else {
                        LoadingIndicatorView.hide()
                        print("\(String(describing: error?.localizedDescription))")
                    }
                }
                
            }else if fileType == "audio"{
                let audioUrl = URL(fileURLWithPath: "your audio local file path")
                AWSS3Manager.shared.uploadAudio(audioUrl: audioUrl, progress: { [weak self] (progress) in
                    guard let strongSelf = self else { return }
                }) { [weak self] (uploadedFileUrl, error) in
                    
                    guard let strongSelf = self else { return }
                    if let finalPath = uploadedFileUrl as? String {
                        print("final path \(finalPath)")
                        let imageAttach = Attachments.init(type: fileType, fileName: "", name: "", number: "", fileLink: finalPath)
                        self?.attachmentFiles.append(imageAttach)
                        LoadingIndicatorView.hide()
                        DispatchQueue.main.async {
                                               self?.attachmentCollectionView.reloadData()
                        }
                    } else {
                        LoadingIndicatorView.hide()
                        print("\(String(describing: error?.localizedDescription))")
                    }
                }
            }
            
         
        }
        
        func attach(attachments:[Attachment])
           {
           
           }
    
}


extension TaskDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func setUpCollectionViewCell()  {
        self.attachmentCollectionView.delegate = self
        self.attachmentCollectionView.dataSource = self
        
        self.attachmentCollectionView.register(UINib(nibName: "ContactAttachmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ContactAttachmentCollectionViewCell")
        
        self.attachmentCollectionView.register(UINib(nibName: "AttachCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AttachCollectionViewCell")
       // self.attachedFiles = [DeviceContact(type: .none)]
        self.attachmentFiles = [Attachments.init(type: .none, fileName: "", name: "", number: "", fileLink: "")]
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         self.attachmentFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let attachment =  self.attachmentFiles[indexPath.row]
        switch attachment.type {
            
        case "contact":
            return self.getContactCell(collectionView, cellForItemAt: indexPath)
        case .none:
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "attachment")
        case "image":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "image")
        case "gallary":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "image")
        case "doucment":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "document")
        case "audio":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "audio")
        case "video":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "video")
        default :
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "document")
        }
    }
    
    func getDefaultAttachemntCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath,type:String) -> UICollectionViewCell {
        
        guard let cell:AttachCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachCollectionViewCell", for: indexPath) as? AttachCollectionViewCell else {
            return UICollectionViewCell(frame: CGRect.zero)
        }
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.deleteAttachment))
        cell.addGestureRecognizer(longPressGesture)

        _ = cell.setUI(type: type)
        return cell
        
    }
    
    
    func getContactCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell  {
        
        guard let cell:ContactAttachmentCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactAttachmentCollectionViewCell", for: indexPath) as? ContactAttachmentCollectionViewCell else {
            return UICollectionViewCell(frame: CGRect.zero)
        }
        
       
         return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
        let attachment =  self.attachmentFiles[indexPath.row]
           
           switch attachment.type {
               
           case "contact":
              
           break
           case "image":
            self.displayFile(attachment: attachment)
           // self.deleteAttachment(title: "Image", indexpath: indexPath)
               break
           case "gallary":
            self.displayFile(attachment: attachment)
                break
           case "doucment":
            self.displayFile(attachment: attachment)
           // self.deleteAttachment(title: "Document", indexpath: indexPath)
                 break
           case "audio":
            self.displayFile(attachment: attachment)
            //self.deleteAttachment(title: "Audio", indexpath: indexPath)
                break
           case "video":
            self.displayFile(attachment: attachment)
            //self.deleteAttachment(title: "Video", indexpath: indexPath)
                break
           case .none:
               let attachmentVC:AttachmentViewController = AttachmentViewController(nibName: attachmentViewController, bundle: nil)
                      attachmentVC.modalPresentationStyle = .overCurrentContext
                      //attachmentVC.presentingVc = self
                        attachmentVC.presentingTaskVC = self
                      attachmentVC.delegateCb = self
                      self.present(attachmentVC, animated: false, completion: nil)
            break
           default :
            self.displayFile(attachment: attachment)
                break
           }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.attachmentCollectionView.bounds.width / 3.2, height: self.attachmentCollectionView.bounds.height)
    }
    
}


extension TaskDetailViewController: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        return self.previewItem as QLPreviewItem
    }
}
