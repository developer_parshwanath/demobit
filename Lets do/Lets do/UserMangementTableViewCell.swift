//
//  UserMangementTableViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 07/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import Alamofire

class UserMangementTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDescription: UILabel!
    @IBOutlet weak var emailIdLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpData(user:Users)  {
        
        self.userNameLabel.text = user.fullName
        self.userDescription.text = user.organization
        self.emailIdLabel.text = user.email
        self.contactLabel.text = user.mobileNo
        
        guard let urlString = user.profilePic else {
            return
        }
        
     let serverUrl = URL(fileURLWithPath: urlString )
        
       let destination: DownloadRequest.Destination = { _, _ in
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent("pig.png")

            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        AF.download(serverUrl, to: destination).response { response in
            print(response)
            guard let destinationUrl = response.fileURL?.path else
            {
                return
            }
            let image = UIImage(contentsOfFile: destinationUrl)
            self.profileImageView.image = image
        }
       
    }
}
