//
//  LoginViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var emailIdTextField:UITextField!
    @IBOutlet weak var passwordTextField:UITextField!
    
    @IBOutlet weak var emailIdView:UIView!
    @IBOutlet weak var passwordView:UIView!
    
    @IBOutlet weak var nextBtn:UIButton!
    @IBOutlet weak var forgotPasswordBtn:UIButton!
    
    var userDetails:UserDetails = UserDetails()
    var userLogin:UserLogin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // emailIdTextField.text = "dipalikakde@gmail.com"
        //passwordTextField.text = "j8pGyt"
        
        self.registerTextField()
        self.setUI()
        userDetails = UserDetails()
        userDetails.isValidUser = LoginService.checkIsValiduser()
        userDetails.deviceType = "ios"
        self.userDetails.userName = "dipalikakade@gmail.com"
        self.userDetails.password = "ezTJoV"
        self.toggelUI()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.keyBoardNotificationRegistration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.keyBoardNotificationRemove()
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func nextBtnClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.userDetails.isValidUser ? self.login() : self.verifyUser()
        
    }
    
    @IBAction func forgotPasswordClcik(_ sender: UIButton) {
        
        self.forgotPassword()
        
    }
    
    //MARK:- UI Helpers
    func toggelUI()  {
        
        if (UserDefaults.standard.string(forKey: userName) ?? " " != " ") {
            self.nextBtn.setTitle("Login", for: UIControl.State.normal)
            self.forgotPasswordBtn.isHidden = false
            self.passwordView.isHidden = false
            return
        }else{
            self.forgotPasswordBtn.isHidden = true
            self.passwordView.isHidden = true
            
            
            if( self.userDetails.isValidUser)
            {
                self.nextBtn.setTitle("Login", for: UIControl.State.normal)
                self.forgotPasswordBtn.isHidden = false
                self.passwordView.isHidden = false
                return
            }
        }
        
        
        
    }
    
    func setUI()  {
        
        
        
        self.nextBtn.layer.cornerRadius = self.nextBtn.bounds.height/2
        
        self.emailIdView.layer.cornerRadius = self.emailIdTextField.bounds.height/2
        
        self.passwordView.layer.cornerRadius = self.passwordTextField.bounds.height/2
        
        emailIdView.layer.borderColor = UIColor(named: "LightFontColor")?.cgColor
        emailIdView.layer.borderWidth = 0.3
        passwordView.layer.borderWidth = 0.3
        passwordView.layer.borderColor = UIColor(named: "LightFontColor")?.cgColor
        
        self.emailIdTextField.setUpLeftIcon(image: UIImage(named: "Email"))
        self.passwordTextField.setUpLeftIcon(image: UIImage(named: "Lock"))
        
        
        self.passwordTextField.setUpRightName(name: "SHOW", selectedName: "HIDE", color: UIColor(named: "OrangeText") ?? UIColor.clear, selectedColor: UIColor(named: "OrangeText") ?? UIColor.clear)
        
        if let button = self.passwordTextField.rightView as? UIButton
        {
            button.addTarget(self, action: #selector(passwordShowBtnClick), for: UIControl.Event.touchUpInside)
        }
        
    }
    
    func setRootControler()
    {
        let taskVC:TaskHomeViewController = TaskHomeViewController(nibName: "TaskHomeViewController", bundle: nil)
        taskVC.userDetails = userLogin
        let navigationControler:UINavigationController = UINavigationController(rootViewController: taskVC)
        self.view.window?.rootViewController = navigationControler
        self.view.window?.makeKeyAndVisible()
        
    }
    
    @objc func passwordShowBtnClick(sender: UIButton)  {
        
        sender.isSelected = !sender.isSelected
        
        self.passwordTextField.isSecureTextEntry = !sender.isSelected
        
    }
    
    
    
    
    
}

extension LoginViewController:UITextFieldDelegate
{
    
    func registerTextField()  {
        self.emailIdTextField.delegate = self
        self.passwordTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        self.updateRegistrationModel(textField: textField)
    }
    
    
    func updateRegistrationModel(textField: UITextField) {
        switch textField {
        case self.emailIdTextField:
            self.userDetails.userName = textField.text
            break
        case self.passwordTextField:
            self.userDetails.password = textField.text
            
        default:
            print("no case found while updateRegistrationModel")
        }
    }
}


//MARK:- Key Board Handling
extension LoginViewController
{
    func keyBoardNotificationRegistration()  {
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.keyboardDidShow(notification:)),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(self.keyboardDidHide(notification:)),
                                               name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    func keyBoardNotificationRemove()  {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    //MARK: Methods to manage keybaord
    @objc func keyboardDidShow(notification: NSNotification) {
        let info = notification.userInfo
        let keyBoardSize = info![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        self.scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
        
    }
    
    @objc func keyboardDidHide(notification: NSNotification) {
        
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
}

//MARK:- API Calling
extension LoginViewController
{
    
    
    func verifyUser()  {
        
        let message = self.userDetails.validateUserName()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            return
        }
        
        LoginService.login(userDetails:self.userDetails, entity: "user/checkUser", completion: { (userLogin, message,status) in
            
            if !status || userLogin == nil
            {
                self.displayToast(message:message)
            }else{
                
                self.displayToast(message:message)
                self.userDetails.isValidUser = true
                LoginService.setValiduser(user: self.userDetails)
                //self.view.removeChildView()
                self.toggelUI()
            }
        })
        
    }
    
    func login() {
        
        let message = self.userDetails.validateLoginCredentials()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            return
        }
        
        LoginService.login(userDetails:self.userDetails, entity: "user/login", completion: { (userLogin, message,status) in
            
            if !status || userLogin == nil || userLogin?.count ?? 0 < 1
            {
                self.displayToast(message:message)
            }else{
                
                if let userLoginDetails = userLogin?.first
                {
                    self.userLogin = userLoginDetails
                    LoginService.saveNewTockenData(tokenString: userLoginDetails.token, tockenKey: TOKEN)
                    LoginService.setLoginUser(loginUser: userLoginDetails)
                    UserDefaults.standard.set(true, forKey: alreadyLogin)
                    UserDefaults.standard.set(userLoginDetails.emailId ?? " ", forKey: userName)
                    let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
                    let FCMModel = FCMToken.init(userName: userLoginDetails.emailId, fcmToken: fcmToken ?? " ")
                    
                    FCMServices.fcmTokenSubmissin(fcmToken: FCMModel, entity: "user/addFcmToken") { (userData, message,status) in
                        
                        if !status || userData == nil
                        {
                            self.displayToast(message:message)
                            
                        }else{
                            
                            self.setRootControler()
                            
                        }
                    }
                    
                    
                }else{
                    self.displayToast(message: "Failed to login")
                    
                }
                
            }
        })
        
    }
    
    func forgotPassword() {
        
        let message = self.userDetails.validateUserName()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            return
        }
        
        LoginService.login(userDetails:self.userDetails, entity: "user/forgotPwd",  completion: { (userLogin, message,status) in
                   
                   if !status || userLogin == nil
                   {
                       self.displayToast(message:message)
                   }else{
                       
                       self.displayToast(message:message)
                   }
               })
        
    }
}

