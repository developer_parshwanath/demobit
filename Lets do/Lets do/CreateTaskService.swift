//
//  CreateTaskService.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation


struct  CreateTaskService{
    
    static func submitTask(createTask:CreateTask,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity:entity, action: .post, queryParameters: nil,reqBody: createTask.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            
             LoadingIndicatorView.hide()
                       if(status)
                       {
                           completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                           return
                       }else
                       {
                           completion( nil,dataDict?["message"] as? String ?? "",status)
                       }
        }
        
        
     
            
            }
}
