//
//  CreateTask.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct CreateTask{
    var entity:String?
    var orgId:String?
    var userName:String?
    var assignee:String?
    var taskName:String?
    var taskDesc:String?
    var priority:String?
    var status:String?
    var dueDate:String?
    var attachments:[Attachments]? // attchments array
    var comments:[Comments]?
    var connectedContacts:[ConnectedContacts]?
    
 
    func encodeRequest() -> [String:Any]
    {
        return [
            "entity" :entity ?? "",
            "orgId":orgId ?? "" ,
            "username":userName ?? "" ,
            "assignee":assignee ?? "" ,
            "taskName":taskName ?? "" ,
            "taskDesc":taskDesc ?? "" ,
            "priority":priority ?? "" ,
           "status" :status ?? "",
            "dueDate":dueDate ?? "" ,
            "attachments":attachments?.compactMap({ (attachment) -> [String:Any]? in
               return attachment.encodeRequest()
            }) ?? [] ,
            "comments":comments?.compactMap({ (comment) -> [String:Any]? in
               return comment.encodeRequest()
            }) ?? [],
            "connectedContacts":connectedContacts?.compactMap({ (contacts) -> [String:Any]? in
               return contacts.encodeRequest()
            }) ?? []
            
            
            
        ]
        
    }
    
    
}

struct Attachments {
    var type:String?
    var fileName:String?
    var name:String?
    var number:String?
    var fileLink:String?
    
   
    
    static func decodeResponse(responseData:[[String:Any]]?)-> [Attachments]?
    {
        guard responseData != nil else{return nil}
        var userArray:[Attachments] = []
        for dataDict in responseData ?? [] {
            var userLoginDetails = Attachments()
            userLoginDetails.type = dataDict["type"] as? String
            userLoginDetails.fileName = dataDict["fileName"] as? String
            userLoginDetails.name = dataDict["name"] as? String
            userLoginDetails.number = dataDict["number"] as? String
            userLoginDetails.fileLink =  dataDict["fileLink"] as? String
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
    
    func encodeRequest() -> [String:Any]
    {
        return [
            
            "type":type ?? "",
            "fileName":fileName ?? "",
            "name":name ?? "",
            "number":number ?? "",
            "fileLink":fileLink  ?? ""
            
        ]
    }
}

struct Comments {
    var commentDescription:String?
    
    static func decodeResponse(responseData:[[String:Any]]?)-> [Comments]?
    {
        guard responseData != nil else{return nil}
        var userArray:[Comments] = []
        for dataDict in responseData ?? [] {
            var userLoginDetails = Comments()
            userLoginDetails.commentDescription = dataDict["commentDesc"] as? String
            userArray.append(userLoginDetails)
            
        }
        return userArray
    }
    
    func encodeRequest() -> [String:Any]
    {
        return [
            
            "commentDescription":commentDescription ?? ""
        ]
    }
}

struct ConnectedContacts {
    var userName:String?
   
    
    func encodeRequest() -> [String:Any]
    {
        return [
            
            "username":userName ?? ""
        ]
    }
}
