//
//  ProfileViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 6/27/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var roleLabel:UILabel!
    @IBOutlet weak var contactLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    
    @IBOutlet weak var coverImageView:UIImageView!
    @IBOutlet weak var profileImageView:UIImageView!
    @IBOutlet weak var profileView:UIView!
    
    var profileDetails:UserProfile?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        fetchProfile()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        self.navigationController?.navigationBar.isHidden = false
        //        self.navigationController?.navigationBar.isTranslucent = true
        //self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    func setUpUI()  {
        
        self.profileView.layer.cornerRadius = self.profileView.bounds.height/2
        
        self.profileImageView.layer.cornerRadius =  self.profileImageView.bounds.height/2
        
        self.profileView.layer.borderColor = UIColor(named: "LightFontColor")?.cgColor
        self.profileView.layer.borderWidth = 0.3
        
        self.profileView.layer.shadowColor = UIColor(named: "LightFontColor")?.cgColor
        self.profileView.layer.shadowRadius = 0.8
        
        
        
    }
    
    
    @IBAction func backBtnClcik(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editProfileClick(_ sender: UIButton) {
        
        editProfile()
    }
    
    func setUpProfileData()  {
        
        self.nameLabel.text = profileDetails?.name ?? ""
        self.roleLabel.text = profileDetails?.role ?? ""
        self.contactLabel.text = profileDetails?.mobileNo
        self.emailLabel.text = profileDetails?.email
        //        self.profileImageView
        //        self.coverImageView
    }
    
}

extension ProfileViewController
{
    private func fetchProfile()  {
        
        let loginUser =  LoginService.getLoginUser()
        
        let fetchProfile = FetchProfile(username: loginUser.emailId ?? "")
        
        ProfileShowService.fetchUserProfile(loginUser: fetchProfile, entity: "user/profile") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                
                self.displayToast(message:message)
                self.profileDetails = data?.first
                self.setUpProfileData()
                
            }
        }
        
    }
    
    private func editProfile()  {
        
        let loginUser = EditProfile.init(token:  LoginService.getLoginUser().token ?? " ", username:  LoginService.getLoginUser().emailId ?? " ", name:  LoginService.getLoginUser().name ?? " ", mobileNo:  LoginService.getLoginUser().mobileNumber ?? " ", profilePic:  LoginService.getLoginUser().profilePic ?? " ")
        
        
        ProfileShowService.editUserProfile(loginUser: loginUser, entity: "user/editProfile") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                self.displayToast(message:message)
            }else{
                
                self.displayToast(message:message)
                self.profileDetails = data?.first
                self.setUpProfileData()
                
            }
        }
    }
}
