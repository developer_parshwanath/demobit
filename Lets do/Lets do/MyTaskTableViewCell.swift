//
//  MyTaskTableViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 01/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class MyTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var moreViewWidth: NSLayoutConstraint!
    @IBOutlet weak var moreViewHeight: NSLayoutConstraint!
    @IBOutlet weak var waitingView: UIView!
    
    //Uilabel for tsk details
    
    @IBOutlet weak var taskNumberLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel: UILabel!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var taskAssignedByLabel: UILabel!
    @IBOutlet weak var taskCompletedLabel: UILabel!
    @IBOutlet weak var waitingForApprovalLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    
    
    var myTask:Bool?
    var dictodTask:TaskList?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        waitingView.roundCorners(corners: [.topRight,.bottomRight], radius: 10)
        
        
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func onMoreButtonClick(_ sender: Any) {
        
        UIView.animate(withDuration: 5.6, animations: { () -> Void in
            if self.moreViewHeight.constant == 0{
                self.moreViewHeight.constant = 140
                self.moreViewWidth.constant = 100
            }else{
                self.moreViewHeight.constant = 0
                self.moreViewWidth.constant = 0
            }
        })
    }
    
    func setData(dictofData:TaskList) -> MyTaskTableViewCell {
        self.taskNumberLabel.text = dictofData.taskNumber ?? " "
        self.taskNameLabel.text = dictofData.taskName ?? " "
        self.taskDescriptionLabel.text = dictofData.taskDesc ?? " "
        if myTask ?? false {
            self.taskAssignedByLabel.text = "Assigned To : \(dictofData.assignee ?? " ")"
        }else{
            self.taskAssignedByLabel.text = "Created By : \(dictofData.createdBy ?? " ")"
        }
        self.taskCompletedLabel.text = "Completed On : \(dictofData.createdDate ?? " ")"
        self.priorityLabel.text = "Priority :\((dictofData.priority ?? " ").capitalizingFirstLetter())"
        self.waitingForApprovalLabel.text = (dictofData.status ?? " ").capitalizingFirstLetter()
        return self
    }
    
    
}
