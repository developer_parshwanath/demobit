//
//  UserServices.swift
//  Lets do
//
//  Created by Darshan Bagmar on 23/08/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation


class UserServices{
    
    static func getUserList(userInfo:FetchUserList,entity:Entity,completion:@escaping(_ responseModel:[Users]?,_ message:String,_ status:Bool)-> Void){
               LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .get, queryParameters: nil,reqBody: userInfo.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN, completionHandler: { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion( Users.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        })
          
          }
    
}
