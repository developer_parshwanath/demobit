//
//  FCMServices.swift
//  Lets do
//
//  Created by Darshan Bagmar on 26/09/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

class FCMServices{
    
    static func fcmTokenSubmissin(fcmToken:FCMToken,entity:Entity,completion:@escaping(_ responseModel:[UserLogin]?,_ message:String,_ status:Bool)-> Void){
        
        //        let userDetails:UserDetails = UserDetails(userName: "dipalikakade@gmail.com", password: "", isValidUser: false)
        LoadingIndicatorView.show()
        networkRequestWith(entity:entity, action: .post, queryParameters: nil,reqBody: fcmToken.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion( UserLogin.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
    }
    
}
