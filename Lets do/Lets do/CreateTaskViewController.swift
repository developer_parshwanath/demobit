//
//  CreateTaskViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
protocol TaskAttachment : NSObject {
    
    func attach(attachments:[Attachment])
    func addFile(fileType:String,fileUrl:NSURL,image:UIImage)
}

class CreateTaskViewController: UIViewController {
    
    var userLogin:UserLogin?
    @IBOutlet weak var taskDescriptionLabel: UILabel!
   
    
    
   
    @IBOutlet weak var taskDesciptionTextArea: UITextView!
   
    
    @IBOutlet var createTaskView: UIView!
    @IBOutlet weak var taskNoView: UIView!
    @IBOutlet weak var createdByView: UIView!
    @IBOutlet weak var createdDateTimeView: UIView!
    @IBOutlet weak var taskNameView: UIView!
    @IBOutlet weak var taskDescriptionView: UIView!
    @IBOutlet weak var assigneeView: UIView!
    @IBOutlet weak var connectedContactView: UIView!
    @IBOutlet weak var priorityView: UIView!
    var delegate:TaskAttachment!
    
    @IBOutlet weak var taskNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var taskCreatdByTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var taskDateTimeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var taskNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var assigneeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var connectedContactTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var priorityTextField: SkyFloatingLabelTextField!
    
   
    var scrollView: UIScrollView!
    @IBOutlet weak var attachmentCollectionview: UICollectionView!
    
    var attachedFiles:[Attachment] = []
    var attachmentFiles:[Attachments] = []
    var assigneeContact:Contact?
    var connecteContact:[ConnectedContacts] = [ConnectedContacts].init()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToolbar()
        self.setCornerRadius()
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 100, width: screenWidth, height: screenHeight - 90))
        

        scrollView.addSubview(self.createTaskView)
       // scrollView.contentSize = CGSize(width: screenWidth - 5, height: self.createTaskView.frame.height)
        self.setUpViewHeight()
        self.setTextFieldDelegate()
        view.addSubview(scrollView)
        self.setUpCollectionViewCell()
       

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.taskCreatdByTextField.text = userLogin?.name
        self.taskDateTimeTextField.text = Date().shortDateTime
    }

    func setUpViewHeight() {
        
        self.createTaskView.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: 970.0)
        
        let height:CGFloat = createTaskView.bounds.height;
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width - 40, height: height)
        
    }

    
    
    
    func setCornerRadius(){
      
       // taskDescriptionView.fadedShadow()
        taskDescriptionView.roundedCornersWithBorder()
        
        
        
    }
    
    
    func setTextFieldDelegate(){
        self.taskNameTextField.delegate = self
        self.taskCreatdByTextField.delegate = self
        self.taskDateTimeTextField.delegate = self
        self.assigneeTextField.delegate = self
        self.connectedContactTextField.delegate = self
        self.priorityTextField.delegate = self
        
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .curveEaseIn, animations: {
            view.isHidden = hidden
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
//    @IBAction func onAttachmentButtonClick(_ sender: Any) {
//        let attachmentVC:AttachmentViewController = AttachmentViewController(nibName: attachmentViewController, bundle: nil)
//        attachmentVC.modalPresentationStyle = .overCurrentContext
//        attachmentVC.presentingVc = self
//        self.present(attachmentVC, animated: false, completion: nil)
//
//    }
    
    
    @IBAction func onSubmitTaskClick(_ sender: Any) {
        self.submitTask()
    }
    
    func submitTask(){
        
        let attachment = self.attachmentFiles[0]
        if attachment.type == .none {
            self.attachmentFiles.remove(at: 0)
        }
        
       // https://ap-south-1.linodeobjects.com/letsdo/7856C559-6278-47C1-80DF-AF2E0EC61151-1132-0000011D20FAF004.jpeg
        let attach = Attachments.init(type: "image", fileName: "test.png", name: "test", number: "", fileLink: "https://ap-south-1.linodeobjects.com/letsdo/0000011D20FAF004.jpeg")
        attachmentFiles.removeAll()
        attachmentFiles.append(attach)
        
        var createTask:CreateTask = CreateTask.init(entity: "createTask", orgId: userLogin?.orgId, userName: userLogin?.emailId, assignee: assigneeContact?.email, taskName: taskNameTextField.text, taskDesc: taskDesciptionTextArea.text, priority: priorityTextField.text, status: "pending", dueDate: taskDateTimeTextField.text, attachments: nil, comments: nil, connectedContacts: connecteContact)
        
        if assigneeContact?.email == nil || assigneeContact?.email == " "{
            createTask.status = "unassigned"
        }
        
        print("create task \(createTask)")
        CreateTaskService.submitTask(createTask: createTask, entity: "user/createTask") { (profileData, message,status) in
            
            if status
            {
                self.displayToast(message: "Task Created")
                self.navigationController?.popViewController(animated: true)
            }else {
                self.displayToast(message: "Task Creation fail please try again")
            }
            
        }
    }

    @IBAction func onBackButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupToolbar(){
    //Create a toolbar
    let bar = UIToolbar()
    //Create a done button with an action to trigger our function to dismiss the keyboard
    let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissMyKeyboard))
    //Create a felxible space item so that we can add it around in toolbar to position our done button
    let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    //Add the created button items in the toobar
    bar.items = [flexSpace, flexSpace, doneBtn]
    bar.sizeToFit()
    //Add the toolbar to our textfield
        self.taskNameTextField.inputAccessoryView = bar
        self.taskCreatdByTextField.inputAccessoryView = bar
        self.taskDateTimeTextField.inputAccessoryView = bar
        self.assigneeTextField.inputAccessoryView = bar
        self.connectedContactTextField.inputAccessoryView = bar
        self.priorityTextField.inputAccessoryView = bar
        
        
    }
    @objc func dismissMyKeyboard(){
    view.endEditing(true)
    }
    
    
    func showAlertWithThreeButton() {
        let alert = UIAlertController(title: "Priority", message: "", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Urgent", style: .default, handler: { (_) in
            self.priorityTextField.text = "Urgent"
            self.dismiss(animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "High", style: .default, handler: { (_) in
            self.priorityTextField.text = "High"
            self.dismiss(animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "Normal", style: .default, handler: { (_) in
            self.priorityTextField.text = "Normal"
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteAttachment(title:String,indexpath:IndexPath) {
        let alert = UIAlertController(title: title, message: "Delete attachment", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (_) in
            self.attachmentFiles.remove(at: indexpath.row)
            DispatchQueue.main.async {
                self.attachmentCollectionview.reloadData()
            }
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}



extension CreateTaskViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == assigneeTextField {
             let contactVC:ContactListViewController = ContactListViewController(nibName: "ContactListViewController", bundle: nil)
            contactVC.userLogin = userLogin
           contactVC.isMultipleselction = false
            contactVC.callBack = { model in
                guard  let contact = model as? Contact else {
                    return
                }
                print(contact)
                self.assigneeTextField.text = contact.name
                self.assigneeContact = contact
                
            }
            //self.navigationController?.pushViewController(contactVC, animated: true)
            self.present(contactVC, animated: false, completion: nil)
        }else if textField == connectedContactTextField {
            self.connecteContact.removeAll()
             let contactVC:ContactListViewController = ContactListViewController(nibName: "ContactListViewController", bundle: nil)
            contactVC.userLogin = userLogin
           contactVC.isMultipleselction = true
            contactVC.callBack = { model in
                guard  let contact = model as? [Contact] else {
                    return
                }
                print(contact)
                
                var nameString:String?
                if contact.count > 0 {
                    for data in contact {
                        let con = ConnectedContacts.init(userName: data.email)
                        self.connecteContact.append(con)
                        if nameString != " "{
                            nameString = data.name
                            self.connectedContactTextField.text = nameString ?? " "
                        }else{
                            nameString! += "," + data.name!
                            self.connectedContactTextField.text = nameString ?? " "
                        }
                    }
                }
               
            }
            //self.navigationController?.pushViewController(contactVC, animated: true)
            self.present(contactVC, animated: false, completion: nil)
        }
        
        else if textField == priorityTextField{
            self.showAlertWithThreeButton()
        }
       
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension CreateTaskViewController:TaskAttachment
{
    func addFile(fileType: String, fileUrl: NSURL, image:UIImage) {

        if fileType == "image" {
            //guard let image = image else { return } //1
            AWSS3Manager.shared.uploadImage(image: image, progress: {[weak self] ( uploadProgress) in
                
                guard let strongSelf = self else { return }
              //  strongSelf.progressView.progress = Float(uploadProgress)//2
                
            }) {[weak self] (uploadedFileUrl, error) in
                
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String { // 3
                  print("final path \(finalPath)")
                    let imageAttach = Attachments.init(type: fileType, fileName: "test.png", name: "", number: "", fileLink: finalPath)
                    self?.attachmentFiles.append(imageAttach)
                    DispatchQueue.main.async {
                        self?.attachmentCollectionview.reloadData()
                         }
                } else {
                    print("\(String(describing: error?.localizedDescription))") // 4
                }
            }
        }else if fileType == "video"{
            let videoUrl = fileUrl.fileReferenceURL()!
            AWSS3Manager.shared.uploadVideo(videoUrl: videoUrl, progress: { [weak self] (progress) in
               
                
            }) { [weak self] (uploadedFileUrl, error) in
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    print("final path \(finalPath)")
                    let imageAttach = Attachments.init(type: fileType, fileName: "", name: "", number: "", fileLink: finalPath)
                    self?.attachmentFiles.append(imageAttach)
                    DispatchQueue.main.async {
                                           self?.attachmentCollectionview.reloadData()
                    }
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                }
            }
            
        }else if fileType == "audio"{
            let audioUrl = URL(fileURLWithPath: "your audio local file path")
            AWSS3Manager.shared.uploadAudio(audioUrl: audioUrl, progress: { [weak self] (progress) in
                guard let strongSelf = self else { return }
            }) { [weak self] (uploadedFileUrl, error) in
                
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    print("final path \(finalPath)")
                    let imageAttach = Attachments.init(type: fileType, fileName: "", name: "", number: "", fileLink: finalPath)
                    self?.attachmentFiles.append(imageAttach)
                    DispatchQueue.main.async {
                                           self?.attachmentCollectionview.reloadData()
                    }
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                }
            }
        }
        
     
    }
    
    func attach(attachments:[Attachment])
       {
        let attachemnt =  self.self.attachedFiles.popLast()
        self.attachedFiles.append(contentsOf: attachments)
        
        if attachemnt != nil
        {
             self.attachedFiles.append(attachemnt!)
        }
       
        self.attachmentCollectionview.reloadData()
       }
}


extension CreateTaskViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func setUpCollectionViewCell()  {
        self.attachmentCollectionview.delegate = self
        self.attachmentCollectionview.dataSource = self
        
        self.attachmentCollectionview.register(UINib(nibName: "ContactAttachmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ContactAttachmentCollectionViewCell")
        
        self.attachmentCollectionview.register(UINib(nibName: "AttachCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AttachCollectionViewCell")
        self.attachedFiles = [DeviceContact(type: .none)]
        self.attachmentFiles = [Attachments.init(type: .none, fileName: "", name: "", number: "", fileLink: "")]
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         self.attachmentFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let attachment =  self.attachmentFiles[indexPath.row]
        switch attachment.type {
            
        case "contact":
            return self.getContactCell(collectionView, cellForItemAt: indexPath)
        case .none:
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "attachment")
        case "image":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "image")
        case "gallary":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "image")
        case "doucment":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "document")
        case "audio":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "audio")
        case "video":
            return self.getDefaultAttachemntCell(collectionView, cellForItemAt: indexPath, type: "video")
        default :
             return UICollectionViewCell(frame: CGRect.zero)
        }
    }
    
    func getDefaultAttachemntCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath,type:String) -> UICollectionViewCell {
        
        guard let cell:AttachCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachCollectionViewCell", for: indexPath) as? AttachCollectionViewCell else {
            return UICollectionViewCell(frame: CGRect.zero)
        }
        
        _ = cell.setUI(type: type)
        return cell
        
    }
    
    
    func getContactCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell  {
        
        guard let cell:ContactAttachmentCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactAttachmentCollectionViewCell", for: indexPath) as? ContactAttachmentCollectionViewCell else {
            return UICollectionViewCell(frame: CGRect.zero)
        }
        if let attachment:DeviceContact =  self.attachedFiles[indexPath.row] as? DeviceContact
        {
            cell.nameLabel.text = attachment.name
        }
        
       
         return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
        let attachment =  self.attachmentFiles[indexPath.row]
           
           switch attachment.type {
               
           case "contact":
              
           break
           case "image":
            self.deleteAttachment(title: "Image", indexpath: indexPath)
               break
           case "gallary":
                break
           case "doucment":
            self.deleteAttachment(title: "Document", indexpath: indexPath)
                 break
           case "audio":
            self.deleteAttachment(title: "Audio", indexpath: indexPath)
                break
           case "video":
            self.deleteAttachment(title: "Video", indexpath: indexPath)
                break
           case .none:
               let attachmentVC:AttachmentViewController = AttachmentViewController(nibName: attachmentViewController, bundle: nil)
                      attachmentVC.modalPresentationStyle = .overCurrentContext
                      attachmentVC.presentingVc = self
                      attachmentVC.delegateCb = self
                      self.present(attachmentVC, animated: false, completion: nil)
            break
           default :
                break
           }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.attachmentCollectionview.bounds.width / 3.2, height: self.attachmentCollectionview.bounds.height)
    }
    
}
