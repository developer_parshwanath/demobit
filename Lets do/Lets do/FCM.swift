//
//  FCM.swift
//  Lets do
//
//  Created by Darshan Bagmar on 26/09/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct FCMToken {
    var userName:String?
    var fcmToken:String?
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "username" :userName ?? "",
            "fcmToken":fcmToken ?? ""
            
        ]
        
    }
}
