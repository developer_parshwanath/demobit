//
//  ContactAttachmentCollectionViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 7/9/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class ContactAttachmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
