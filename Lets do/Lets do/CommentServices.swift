//
//  CommentServices.swift
//  Lets do
//
//  Created by Darshan Bagmar on 16/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct  CommentService{
    
    static func addCommentService(addComment:AddComment,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: addComment.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                  completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
        
    }
    
    static func editCommentService(editComment:EditComment,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: editComment.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
    }
    
    static func deleteCommentService(deleteComment:DeleteComment,entity:Entity,completion:@escaping(_ responseMode:[UserProfile]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .post, queryParameters: nil,reqBody: deleteComment.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion(UserProfile.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        
        
        
    }
    
    static func fetchCommentList(fetchComment:FetchCommentList,entity:Entity,completion:@escaping(_ responseMode:[CommentList]?,_ message:String,_ status:Bool)-> Void){
        LoadingIndicatorView.show()
        networkRequestWith(entity: entity, action: .get, queryParameters: nil,reqBody: fetchComment.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
             LoadingIndicatorView.hide()
            if(status)
            {
                completion(CommentList.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                 completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
    }
}
