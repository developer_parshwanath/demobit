//
//  ContactTableViewCell.swift
//  Lets do
//
//  Created by Darshan Bagmar on 7/8/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactNumberlabel: UILabel!
    @IBOutlet weak var selectbtn: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
