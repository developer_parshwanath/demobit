//
//  AddUserViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 07/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class AddUserViewController: UIViewController {
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailIdTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var contactNumbereTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var userPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var entityType:Entity = "admin/addUser"
    var userDetail:Users?
    
    var saveUser:SaveUser?
    var editUser:EditUser?
    var deleteUser:DeleteUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeModel()
        self.registerTextField()
        
    }
    
    func initializeModel()  {
        
        switch self.entityType {
        case "admin/addUser":
            self.saveUser = SaveUser()
          let loggedInUser = LoginService.getLoginUser()
            self.saveUser?.orgId = loggedInUser.orgId
            self.saveUser?.createdBy = loggedInUser.emailId
            self.saveBtn.setTitle("SAVE", for: UIControl.State.normal)
            self.deleteBtn.setTitle("SAVE & NEXT", for: UIControl.State.normal)
            return
        case "admin/editUser":
            self.editUser = EditUser()
            self.deleteUser = DeleteUser()
            self.setUpDetails()
            self.saveBtn.setTitle("UPDATE", for: UIControl.State.normal)
            self.deleteBtn.setTitle("DELETE", for: UIControl.State.normal)
            return
        default:
            return
        }
    }
    
    func setUpDetails()  {
        
       
        let fullNameArray = self.userDetail?.fullName?.split(separator: " ")
        if (fullNameArray?.count ?? 0 > 0)
        {
           
            let firstName = "\(fullNameArray?[0] ?? "")"
            let lastName =  (fullNameArray?.count ?? 0) > 1 ? "\(fullNameArray?[1] ?? "" )" : ""
            let emailId = self.userDetail?.email
            let mobileNumber = self.userDetail?.mobileNo
            self.firstNameTextField.text = firstName
            self.lastNameTextField.text = lastName
            self.editUser?.firstName = firstName
            self.editUser?.lastName = lastName
            self.editUser?.username = emailId
           
            self.deleteUser?.delUser = emailId
            self.emailIdTextField.text = emailId
            self.contactNumbereTextField.text = mobileNumber
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.keyBoardNotificationRegistration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.keyBoardNotificationRemove()
    }
    
    @IBAction func onBackButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnClick(_ sender: UIButton) {
        
        switch self.entityType {
        case "admin/addUser":
             self.addUser()
            return
        case "admin/editUser":
            self.updateUser()
            return
        default:
            return
        }
        
    }
    
    @IBAction func saveAndUpdate(_ sender: UIButton) {
        
        switch self.entityType {
        case "admin/addUser":
            self.addUser()
            return
        case "admin/editUser":
            self.removeUser()
            return
        default:
            return
        }
    }
    
    private func addUser()
    {
       let message = self.saveUser?.validateLoginCredentials()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            return
        }
        
        UserService.addUser(userDetails: self.saveUser!, entity: "admin/addUser") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                self.displayToast(message:message)
            }else{
                self.navigationController?.popViewController(animated: true)
                self.displayToast(message:message)
            }
        }
        
    }
    
    private func updateUser()
    {
       let message = self.editUser?.validateLoginCredentials()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            return
        }
        
        UserService.editUser(userDetails: self.editUser!, entity: "admin/editUser") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                self.displayToast(message:message)
            }else{
                self.navigationController?.popViewController(animated: true)
                self.displayToast(message:message)
            }
        }
        
    }
    
    private func removeUser()
    {
       let message = self.deleteUser?.validateLoginCredentials()
        
        if(message != nil)
        {
            self.displayToast(message: message!)
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        UserService.deleteUser(userDetails: self.deleteUser!, entity: "admin/editUser") { (data, message,status) in
            
            if !status ||  data == nil || data?.count ?? 0 < 1
            {
                self.displayToast(message:message)
            }else{
                self.displayToast(message:message)
            }
        }
    }
}

//MARK:- Key Board Handling
extension AddUserViewController
{
    func keyBoardNotificationRegistration()  {
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.keyboardDidShow(notification:)),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(self.keyboardDidHide(notification:)),
                                               name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    func keyBoardNotificationRemove()  {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    //MARK: Methods to manage keybaord
    @objc func keyboardDidShow(notification: NSNotification) {
        let info = notification.userInfo
        let keyBoardSize = info![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        self.scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
        
    }
    
    @objc func keyboardDidHide(notification: NSNotification) {
        
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
}


extension AddUserViewController:UITextFieldDelegate
{
    
    func registerTextField()  {
        self.emailIdTextField.delegate = self
        self.firstNameTextField.delegate = self
        self.lastNameTextField.delegate = self
        self.contactNumbereTextField.delegate = self
        self.userPasswordTextField.delegate = self
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        switch self.entityType {
        case "admin/addUser":
            self.updateSaveUserModel(textField: textField)
            return
        case "admin/editUser":
            self.updateEditUserModel(textField: textField)
            self.deleteUserModel(textField: textField)
            return
        default:
            return
        }
    }
    
    
    func updateSaveUserModel(textField: UITextField) {
        switch textField {
        case self.firstNameTextField:
             self.saveUser?.firstName = textField.text
            break
        case self.lastNameTextField:
            self.saveUser?.lastName = textField.text
            break
        case self.emailIdTextField:
            self.saveUser?.email = textField.text
            break
        case self.contactNumbereTextField:
             self.saveUser?.mobileNo = textField.text
            break
        case self.userPasswordTextField:
            self.saveUser?.password = textField.text
            break
            
        default:
            print("no case found while updateRegistrationModel")
        }
    }
    
    func updateEditUserModel(textField: UITextField) {
        switch textField {
        case self.firstNameTextField:
             self.editUser?.firstName = textField.text
            break
        case self.lastNameTextField:
            self.editUser?.lastName = textField.text
            break
        case self.emailIdTextField:
            self.editUser?.username = textField.text
            break
        case self.userPasswordTextField:
        self.editUser?.password = textField.text
        break
            
        default:
            print("no case found while updateRegistrationModel")
        }
    }
    
    func deleteUserModel(textField: UITextField) {
        switch textField {
            
        case self.emailIdTextField:
             self.deleteUser?.delUser = textField.text
            break
            
        default:
            print("no case found while updateRegistrationModel")
        }
    }
}



struct SaveUser{
    
    var createdBy  :String?
    var orgId      :String?
    
    var firstName  :String?
    var lastName   :String?
    var email      :String?
    var password   :String?
    var mobileNo   :String?
    var profilePic :String = ""
    
    var fullName:String = ""
    
    func validateUserName() -> String? {
        
        return self.email == nil || self.email?.isEmpty ?? true || !Validation.isValidEmail(EmailId: self.email ?? "") ? "Please enter valid email id" : nil
    }
    
    mutating func validateLoginCredentials() -> String? {
        
        if(self.firstName == nil || self.firstName?.isEmpty ?? true)
        {
            return "Please enter first name"
            
        }else if(self.lastName == nil || self.lastName?.isEmpty ?? true)
        {
            return "Please enter last name"
            
        }else if(self.validateUserName() != nil)
        {
            return self.validateUserName()
            
        }else if(self.password == nil || self.password?.isEmpty ?? true)
        {
            return "Please enter valid password"
            
        }else if(self.mobileNo == nil || self.mobileNo?.isEmpty ?? true || !Validation.isValidMobileNumber(MobileNumber: self.mobileNo ?? ""))
        {
            return "Please enter valid mobile number"
        }
        
        self.fullName = (self.firstName ?? "") + "" + (self.lastName ?? "")
        
        return nil
        
    }
    
    
    func encodeRequest() -> [String:Any]
    {
        return [
            
            "createdBy" :createdBy ?? "",
            "orgId"     :orgId     ?? "",
            "firstName" :firstName ?? "",
            "lastName"  :lastName  ?? "",
            "email"     :email     ?? "",
            "password"  :password  ?? "",
            "mobileNo"  :mobileNo  ?? "",
            "profilePic":profilePic ,
            "fullName" : fullName
        
            
        ]
        
        
        
    }
    
    
    
}

struct EditUser {
    
    var firstName  :String?
    var lastName   :String?
    var username   :String?
    var password   :String?
    var mobileNo   :String = ""
    var profilePic :String = ""
    
    var fullName:String?  {
        return (firstName ?? "") + "" + (lastName ?? "")
    }
    
    func validateUserName() -> String? {
        
        return self.username == nil || self.username?.isEmpty ?? true ? "Please enter valid email id" : nil
    }
    
    func validateLoginCredentials() -> String? {
        
        if(self.firstName == nil || self.firstName?.isEmpty ?? true)
        {
            return "Please enter first name"
            
        }else if(self.lastName == nil || self.lastName?.isEmpty ?? true)
        {
            return "Please enter last name"
            
        }else if(self.validateUserName() != nil)
        {
            return self.validateUserName()
            
        }else if(self.password == nil || self.password?.isEmpty ?? true)
        {
            return "Please enter valid password"
        }
        
        return nil
    }
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "firstName" : firstName  ?? "" ,
            "lastName" : lastName   ?? "" ,
            "username" : username   ?? "" ,
            "password" : password   ?? "" ,
            "mobileNo" : mobileNo   ,
            "profilePic" : profilePic ,
            "fullName" : fullName ?? ""
            
        ]
        
    }
}

struct DeleteUser {
    
    var delUser  :String?
    
    func validateUserName() -> String? {
        
        return self.delUser == nil || self.delUser?.isEmpty ?? true ? "Please enter valid email id" : nil
    }
    
    func validateLoginCredentials() -> String? {
        
         if(self.validateUserName() != nil)
        {
            return self.validateUserName()
        }
        return nil
    }
    
    func encodeRequest() -> [String:Any]
    {
        return [
            "delUser" : delUser  ?? ""
        ]
        
    }
}
