//
//  NotificationViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var notificationTableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNotificatinCell()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        //self.navigationController?.navigationBar.backgroundColor = .blue
    }
    
    
    @IBAction func onBackButtonClick(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController:UITableViewDelegate,UITableViewDataSource
{
    
    func setUpNotificatinCell()  {
        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        
        self.notificationTableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as? NotificationTableViewCell else {
            
            return UITableViewCell(frame:CGRect.zero)
        }
        
        cell.setUpUI()
        return  cell
    }
    
    
}
