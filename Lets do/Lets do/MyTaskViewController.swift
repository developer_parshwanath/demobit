//
//  MyTaskViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

protocol navigationSet {
    func vcPopup(taskList:TaskList)
}


class MyTaskViewController: UIViewController,UIActionSheetDelegate {

   
    var delegate:navigationSet?
    @IBOutlet weak var myTasksTableVies: UITableView!
    
    @IBOutlet weak var assignedButton: UIButton!
    @IBOutlet weak var unAssignedButton: UIButton!
    @IBOutlet weak var closedButton: UIButton!
    var myTask:[TaskList]?
    var showMyTask:[TaskList]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showMyTask = myTask
        self.setUpMyTaskCell()
        self.myTasksTableVies.reloadData()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.myTasksTableVies.reloadData()
    }
    

    @IBAction func assignedButtonClick(_ sender: Any) {
            assignedButton.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
            assignedButton.setImage(UIImage.init(named: radioButtonSelected), for: .normal)
            
           unAssignedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
             unAssignedButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
            closedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
            closedButton.imageView?.image = UIImage.init(named: radioButtonUnSelected)
            
            showMyTask = myTask//myTask?.filter{$0.status == assigned}
            self.myTasksTableVies.reloadData()
        
    }
    
    @IBAction func unAssignedButtonClick(_ sender: Any) {
           unAssignedButton.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
           unAssignedButton.setImage(UIImage.init(named: radioButtonSelected), for: .normal)
       
           assignedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
           assignedButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
       
           closedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
           closedButton.imageView?.image = UIImage.init(named: radioButtonUnSelected)
           showMyTask = myTask?.filter{$0.status == "UnAssigned"}
            self.myTasksTableVies.reloadData()
    }
    
    @IBAction func closeButtonClick(_ sender: Any) {
        
           closedButton.setTitleColor(UIColor.init(named: headerBlueColor), for: .normal)
           closedButton.imageView?.image = UIImage.init(named: radioButtonSelected)
       
           unAssignedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
           unAssignedButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
       
          assignedButton.setTitleColor(UIColor.init(named: lightFontColor), for: .normal)
           assignedButton.setImage(UIImage.init(named: radioButtonUnSelected), for: .normal)
        
        showMyTask = myTask?.filter{$0.status == "Closed"}
        self.myTasksTableVies.reloadData()
    }
    
    
    @IBAction func onSortButtonClick(_ sender: Any) {
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .actionSheet)
        let actionDueDate = UIAlertAction(title: "Due Date", style: .default) {
            UIAlertAction in
            // Write your code here
            let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                self.showMyTask = self.showMyTask?.sorted{(taskOne, taskTwo) -> Bool in
                    return (dateFormatter.date(from: taskOne.dueDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.dueDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                }
            DispatchQueue.main.async {
                self.myTasksTableVies.reloadData()
            }
        }
        alert.addAction(actionDueDate)
        
        let actionPriority = UIAlertAction(title: "Priority", style: .default) {
                   UIAlertAction in
                   // Write your code here
                    self.showMyTask = self.showMyTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (taskOne.priority ?? " ").compare(taskTwo.priority ?? " ") == ComparisonResult.orderedDescending
                    }
                DispatchQueue.main.async {
                    self.myTasksTableVies.reloadData()
                }
               }
               alert.addAction(actionPriority)
        
        let actionCOD = UIAlertAction(title: "Completed On Date", style: .default) {
                   UIAlertAction in
                   // Write your code here
                let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    self.showMyTask = self.showMyTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (dateFormatter.date(from: taskOne.completionDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.completionDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                    }
                DispatchQueue.main.async {
                    self.myTasksTableVies.reloadData()
                }
            
               }
               alert.addAction(actionCOD)
        
        let actionCD = UIAlertAction(title: "Create Date", style: .default) {
                   UIAlertAction in
                   let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    self.showMyTask = self.showMyTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (dateFormatter.date(from: taskOne.createdDate ?? " ") ?? Date.init()).compare(dateFormatter.date(from: taskTwo.createdDate ?? " ") ?? Date.init()) == ComparisonResult.orderedDescending
                    }
            DispatchQueue.main.async {
                self.myTasksTableVies.reloadData()
            }
                
               }
               alert.addAction(actionCD)
        
        let actionCB = UIAlertAction(title: "Created By", style: .default) {
                   UIAlertAction in
                   // Write your code here
                self.showMyTask = self.showMyTask?.sorted{(taskOne, taskTwo) -> Bool in
                        return (taskOne.createdBy ?? " ").compare(taskTwo.createdBy ?? " ") == ComparisonResult.orderedDescending
                    }
               DispatchQueue.main.async {
                    self.myTasksTableVies.reloadData()
                }
               }
               alert.addAction(actionCB)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
            // It will dismiss action sheet
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    
    }
    
}



extension MyTaskViewController:UITableViewDataSource,UITableViewDelegate{
    
    func setUpMyTaskCell()  {
        self.myTasksTableVies.delegate = self
        self.myTasksTableVies.dataSource = self
        
        self.myTasksTableVies.register(UINib(nibName: myTaskTableViewCell, bundle: nil), forCellReuseIdentifier: myTaskTableViewCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showMyTask?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:MyTaskTableViewCell = tableView.dequeueReusableCell(withIdentifier: myTaskTableViewCell, for: indexPath) as? MyTaskTableViewCell else {
            
            
            
            return UITableViewCell(frame:CGRect.zero)
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.myTask = true
        _ = cell.setData(dictofData: (self.showMyTask?[indexPath.row])!)
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.vcPopup(taskList: (self.showMyTask?[indexPath.row])!)
    }
    
    
    
}
