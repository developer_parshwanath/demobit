//
//  AddUser.swift
//  Lets do
//
//  Created by Darshan Bagmar on 24/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

struct FetchUserList{
    let orgId: String?
    let pageSize: Int?
    let pageNumber: Int?
    
   
    func encodeRequest() -> [String:Any]
    {
        return [
            "pageNumber" :pageNumber ?? 0,
            "orgId":orgId ?? "" ,
            "pageSize":pageSize ?? 0
        ]
        
    }
}


struct Users{
    var organization: String?
    var role: String?
    var email: String?
    var fullName: String?
    var mobileNo: String?
    var profilePic: String?
    
   
    static func decodeResponse(responseData:[String:Any]?)-> [Users]?
       {
           guard responseData != nil else{return nil}
           guard let userDetailArray = responseData?["data"] as? [[String:Any]] else {return nil}
           var userArray:[Users] = []
           for dataDict in userDetailArray {
               var userLoginDetails = Users()
               userLoginDetails.organization = dataDict["organization"] as? String
               userLoginDetails.role = dataDict["role"] as? String
               userLoginDetails.mobileNo = dataDict["mobileNo"] as? String
               userLoginDetails.email = dataDict["email"] as? String
               userLoginDetails.fullName =  dataDict["fullName"] as? String
               userLoginDetails.profilePic = dataDict["profilePic"] as? String
               userArray.append(userLoginDetails)
               
           }
           return userArray
       }

}
