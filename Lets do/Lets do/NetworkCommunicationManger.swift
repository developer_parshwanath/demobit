//
//  NetworkCommunicationManger.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation
import Alamofire


func networkRequestWith(entity:String,action:HTTPMethod,queryParameters:[String:Any]? = nil ,reqBody:[String:Any],isTockenRequired:Bool,tockenKey:String = TOKEN ,completionHandler:@escaping(_ data:[String:Any]?,_ status:Bool)-> Void) -> Void
{
    
   
     guard var urlComponents: URLComponents = URLComponents(string: (MAIN_URL + entity)) else
    {
        print("url is nil");
        return;
    }
    
    guard let url: URL =  urlComponents.url else
    {
        print("url is nil");
        return;
    }
    var httpHeaders:HTTPHeaders =  ["Accept": "application/json"]
    
    
    
    if(isTockenRequired)
    {
        guard let token = LoginService.getToken(tokenKey: tockenKey) else
        {
            completionHandler(nil,false)
            return
        }   
        
         httpHeaders.add(name: TOKEN, value: token)
    }
    
   
    if (Reachability.isConnectedToNetwork())
    {
        AF.request(url,method: action ,parameters: reqBody,headers: httpHeaders)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                debugPrint(response)
            }
            .responseData{ response in
            switch response.result {
            case .success:
                do {
                    guard let responseData = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String: Any] else {
                        
                         completionHandler(nil,false)
                        return
                    }
                    var status = false
                    let statusString = responseData["status"] as? String ?? ""
                    if "SUCCESS".caseInsensitiveCompare(statusString) == .orderedSame
                    {
                        status = true
                    }
                    completionHandler(responseData,true)
                    
                } catch {
                    print(error.localizedDescription)
                }
               
            case .failure(_):
               completionHandler(nil,false)
            }
        }
       
    }else
    {
       completionHandler(nil,false)
    }
    
   
    
}
