//
//  LoadingVC.swift
//  Lets do
//
//  Created by Darshan Bagmar on 9/17/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//



import UIKit

class LoadingIndicatorView {
    
    static var currentOverlay : UIView?
    static var currentOverlayTarget : UIView?
    static var currentLoadingText: String?
    
    static func show(_ loadingText: String = "Loading....") {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow, loadingText: loadingText)
    }
    
    static func show(_ overlayTarget : UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    static func show(_ overlayTarget : UIView, loadingText: String?) {
        // Clear it first in case it was already shown
        hide()
        
        // register device orientation notification
        NotificationCenter.default.addObserver(
            self, selector:
            #selector(LoadingIndicatorView.rotated),
            name: UIDevice.orientationDidChangeNotification,
            object: nil)
        
        // Create the overlay
        let newFrame : CGRect = CGRect.init(x: overlayTarget.frame.origin.x, y: overlayTarget.frame.origin.y, width: (UIApplication.shared.keyWindow?.frame.width)!, height: overlayTarget.frame.height)
        let overlay = UIView(frame: newFrame)
        overlay.center = overlayTarget.center
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubviewToFront(overlay)
        
        // Create and animate the activity indicator
        let indicator = UIActivityIndicatorView()
        
        
        if #available(iOSApplicationExtension 13.0, *) {
            if #available(iOS 13.0, *) {
                indicator.style = UIActivityIndicatorView.Style.large
            } else {
                indicator.style = UIActivityIndicatorView.Style.whiteLarge
                // Fallback on earlier versions
            }
        } else {
            indicator.style = UIActivityIndicatorView.Style.whiteLarge
            // Fallback on earlier versions
        }
        indicator.color = UIColor(named: "HeaderBlue")
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.transform = CGAffineTransform(scaleX: 2, y: 2)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: overlay.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: overlay.centerYAnchor).isActive = true
        // Create label
        if let textString = loadingText {
            let label = UILabel()
            label.text = textString
            label.textColor = UIColor(named: "HeaderBlue")
            label.sizeToFit()
            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 60)
            overlay.addSubview(label)
        }
        
        // Animate the overlay to show
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        overlay.alpha = overlay.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
        
        currentOverlay = overlay
        currentOverlayTarget = overlayTarget
        currentLoadingText = loadingText
    }
    
    static func hide() {
        if currentOverlay != nil {
            
            // unregister device orientation notification
            NotificationCenter.default.removeObserver(self,                                                      name: UIDevice.orientationDidChangeNotification,                                                      object: nil)
            
            currentOverlay?.removeFromSuperview()
            currentOverlay =  nil
            currentLoadingText = nil
            currentOverlayTarget = nil
        }
    }
    
    @objc private static func rotated() {
        // handle device orientation change by reactivating the loading indicator
        if currentOverlay != nil {
            show(currentOverlayTarget!, loadingText: currentLoadingText)
        }
    }
}
