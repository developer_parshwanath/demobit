//
//  ColorConstant.swift
//  Lets do
//
//  Created by Darshan Bagmar on 03/07/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

let borderColor = "BorderColor"
let cellLabelBGColor = "CellLabelBGColor"
let darkFontColor = "DarkFontColor"
let dividerColor = "DividerColor"
let greyFontColor = "GreyFontColor"
let headerBlueColor = "HeaderBlue"
let lightFontColor = "LightFontColor"
let orangeTextColor = "OrangeText"
let vcBGColor = "ViewControllerBGColor"

