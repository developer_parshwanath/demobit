//
//  TaskViewService.swift
//  Lets do
//
//  Created by Darshan Bagmar on 20/08/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import Foundation

class TaskViewService{
    
    static func gettaskView(taskLists:fetchTask,entity:Entity,completion:@escaping(_ responseMode:[TaskView]?,_ message:String,_ status:Bool)-> Void){
             LoadingIndicatorView.show()
        networkRequestWith(entity:entity, action: .get, queryParameters: nil,reqBody: taskLists.encodeRequest(),isTockenRequired: true, tockenKey: TOKEN) { (dataDict, status) in
            LoadingIndicatorView.hide()
            
            if(status)
            {
                completion(TaskView.decodeResponse(responseData: dataDict) ,dataDict?["message"] as! String,status)
                return
            }else
            {
                completion( nil,dataDict?["message"] as? String ?? "",status)
            }
        }
        

          
          }
    
}
