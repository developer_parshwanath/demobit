//
//  TaskHomeViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import PageMenu
import SideMenu

class TaskHomeViewController: UIViewController,navigationSet {
    
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var taskList = [TaskList].init()
    var myTaskList: [TaskList]?
    var assigneeToMeTaskList: [TaskList]?
    var connectedTaskList: [TaskList]?
    
    var userDetails:UserLogin?
    //MARK:- contoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.getTaskList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.getTaskList()
    }
    
    func setHomeView(){
        controllerArray.removeAll()
        let myTaskVC : MyTaskViewController = MyTaskViewController(nibName: "MyTaskViewController", bundle: nil)
        myTaskVC.title = "MY TASKS"
        myTaskVC.delegate = self
        myTaskVC.myTask = myTaskList//taskList?.filter{$0.createdBy == userDetails?.name || $0.assigneeName == userDetails?.name}
        controllerArray.append(myTaskVC)
        
        let assignedTaskVC : AssignedTaskViewController = AssignedTaskViewController(nibName: "AssignedTaskViewController", bundle: nil)
        assignedTaskVC.title = "ASSIGNED TO ME"
        assignedTaskVC.delegate = self
        // assignedTaskVC.assignedTask = taskList?.filter{$0.status == "ASSIGNED"}
        assignedTaskVC.assignedTask = assigneeToMeTaskList
        assignedTaskVC.connectedContactTask = connectedTaskList
        controllerArray.append(assignedTaskVC)
        
        let allTaskVC : AllTasksViewController = AllTasksViewController(nibName: "AllTasksViewController", bundle: nil)
        allTaskVC.allTask = taskList
        allTaskVC.title = "ALL TASKS"
        controllerArray.append(allTaskVC)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor(hexString: "#0066CC")),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor.white),
            .unselectedMenuItemLabelColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)),
            .menuItemFont(UIFont(name: "Roboto-Medium", size: 14.0)!),
            .menuHeight(44.0),
            .menuMargin(0.0),
            .selectionIndicatorHeight(4.0),
            .bottomMenuHairlineColor(UIColor.clear),
            .menuItemWidthBasedOnTitleTextWidth(false),
            .selectedMenuItemLabelColor(UIColor.white),
            .menuItemWidth(self.view.frame.width/3)
        ]
        
        
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top
        let bottomPadding = window?.safeAreaInsets.bottom
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 60.0 + topPadding!, width: self.view.frame.width, height: self.view.frame.height - 60.0 - topPadding! - bottomPadding!), pageMenuOptions: parameters)
        pageMenu?.delegate = self as? CAPSPageMenuDelegate
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        
        self.view.addSubview(pageMenu!.view)
        // Do any additional setup after loading the view.
    }
    //MARK:- onClickMenu
    @IBAction func onClickMenuButton(_ sender: Any) {
        let menuVC:MenuViewController = MenuViewController(nibName: "MenuViewController", bundle: nil)
        menuVC.userLogin = userDetails
        let menu = SideMenuNavigationController(rootViewController: menuVC)
        menu.leftSide = true
        menu.presentationStyle = .menuDissolveIn
        
        present(menu, animated: true, completion: nil)
        
        
    }
    
    //MARK:- onClickCreateTask
    @IBAction func onClickCreateTask(_ sender: Any) {
        let createTaskVC:CreateTaskViewController = CreateTaskViewController(nibName: "CreateTaskViewController", bundle: nil)
        createTaskVC.userLogin = userDetails
        self.navigationController?.pushViewController(createTaskVC, animated: true)
    }
    
    //MARK:- onClickNotificationButton
    @IBAction func onClickNotificationButton(_ sender: Any) {
        let notificationVC:NotificationViewController = NotificationViewController(nibName: "NotificationViewController", bundle: nil)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    func vcPopup(taskList: TaskList) {
        let taskDetailVC:TaskDetailViewController = TaskDetailViewController(nibName: taskDetailViewController, bundle: nil)
        taskDetailVC.taskListDict = taskList
        taskDetailVC.userLogin = userDetails
        self.navigationController?.pushViewController(taskDetailVC, animated: true)
    }
    
    func getTaskList(){
        // let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending")
        let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending", createdBy: userDetails?.emailId, assignedTo: nil, connectedContact: nil, pageNumber: 1, pageSize: 20)
        TaskListService.taskList(taskLists: taskLists, entity: "user/tasks") { (taskList, message,status) in
            
            
            if !status || taskList == nil || taskList?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                  self.taskList.removeAll()
                               self.myTaskList = taskList
                               self.getAssignedTaskList()
            }
        }
        
    }
    
    func getAssignedTaskList(){
        // let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending")
        let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending", createdBy: nil, assignedTo: userDetails?.emailId, connectedContact: nil, pageNumber: 1, pageSize: 20)
        TaskListService.taskList(taskLists: taskLists, entity: "user/tasks") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                // self.displayToast(message:message)
            }else{
                
                self.assigneeToMeTaskList = data
                self.getConnectedTaskList()
                
            }
        }
        
        
    }
    
    func getConnectedTaskList(){
        // let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending")
        let taskLists = TaskLists.init(orgId: userDetails?.orgId, userName: userDetails?.emailId, status: "assigned,unassigned,completed,closed,overdue,pending", createdBy: nil, assignedTo:nil, connectedContact: userDetails?.emailId, pageNumber: 1, pageSize: 20)
        TaskListService.taskList(taskLists: taskLists, entity: "user/tasks") { (data, message,status) in
            
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                 self.connectedTaskList = data
                               self.taskList.append(contentsOf: self.myTaskList!)
                               self.taskList.append(contentsOf: self.assigneeToMeTaskList!)
                               self.taskList.append(contentsOf: self.connectedTaskList!)
                               self.setHomeView()
            }
            
        }
        
    }
    
    
    
}


extension TaskHomeViewController:SideMenuNavigationControllerDelegate{
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}
