//
//  ContactListViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 7/30/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit

class ContactListViewController: UIViewController {

   @IBOutlet weak var tableView:UITableView!
    var userLogin:UserLogin?
    var contactList:[Contact] = []
    var filteredContactList:[Contact] = []
    var callBack:((_ model:Any)->())?
    var isMultipleselction:Bool = false

    @IBOutlet weak var buttonStackView: UIStackView!
    override func viewDidLoad() {
         super.viewDidLoad()
         self.registerTableCell()
        
       self.buttonStackView.arrangedSubviews[0].isHidden = !isMultipleselction
       
        
         // Do any additional setup after loading the view.
        self.getUserList()
       
     }
    
    override func viewWillAppear(_ animated: Bool) {
         //self.navigationController?.navigationBar.isHidden = false
    }

    
    func getUserList(){
        let fetchUser = FetchUserList.init(orgId: userLogin?.orgId, pageSize: 10, pageNumber: 1)
        UserServices.getUserList(userInfo: fetchUser, entity: "admin/users") { (data, message,status) in
            
            if !status || data == nil || data?.count ?? 0 < 1
            {
                //self.displayToast(message:message)
            }else{
                
                for contactData in data ?? []{
                    let contact = Contact.init(name: contactData.fullName,email: contactData.email,isSelected: false)
                    self.contactList.append(contact)
                }
                
                self.contactList = self.contactList.sorted{ (contactOne, contactTwo) -> Bool in
                    
                    return  contactOne.name?.localizedCaseInsensitiveCompare(contactTwo.name ?? "") == ComparisonResult.orderedAscending
                }
                
                self.filteredContactList = self.contactList
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func onCnacelBtnClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onDoneBtnClick(_ sender: UIButton) {
        
        let selectedContact = self.filteredContactList.filter({$0.isSelected})
        
        if let callBack = callBack
        {
            callBack(selectedContact)
            
        }
        
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func oBackButtonClick(_ sender: Any) {
        //self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
}

extension ContactListViewController:UITableViewDelegate,UITableViewDataSource
{
    func registerTableCell()  {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredContactList.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cellIdentifier = "Cell"
           
           guard let cell:ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as? ContactTableViewCell else
           {
               return UITableViewCell(frame: CGRect.zero)
           }
           
           
           //  let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
           let user:Contact = self.filteredContactList[indexPath.row]
           
           cell.nameLabel.text = user.name
          // cell.contactNumberlabel.text =  contact.number
           
           if(user.isSelected)
           {
               cell.selectbtn.image = UIImage(named: "CheckBox")
            
           }else{
               
            cell.selectbtn.image = nil
           }
           
          // cell.selectbtn.tag = indexPath.row
        //cell.selectbtn.addTarget(self, action: #selector(onSelectContact), for: UIControl.Event.touchUpInside)
           
           return cell;
       }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (isMultipleselction)
        {
            var deviceContact = self.filteredContactList[indexPath.row]
                    deviceContact.isSelected = !deviceContact.isSelected
                    
                 self.filteredContactList[indexPath.row] = deviceContact
                    self.tableView.reloadData()
            
        }else {
            
            let contactData = self.filteredContactList[indexPath.row]
            
            if let callBack = callBack
            {
                callBack(contactData)
                
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
    
        
    }
       @objc func onSelectContact(sender: UIButton)  {
           
        
       }
    
    
}

extension ContactListViewController:UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if  !searchText.isEmpty && searchText.count > 2
        {
            let filteredOntect =   self.contactList.filter { (contact) -> Bool in
                return (contact.name ?? "").uppercased().contains(searchText.uppercased())
            }
            
            self.filteredContactList = filteredOntect
            self.tableView.reloadData()
            
        }
        else{
            
            self.filteredContactList = contactList
            self.tableView.reloadData()
        }
    }
}

struct Contact {
    var name:String?
    var email:String?
    var isSelected:Bool = false
}
